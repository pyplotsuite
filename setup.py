#!/usr/bin/env python

from distutils.core import setup
from pyplotsuite.common.version import read_version

LONG_DESCRIPTION="""
*PyPlotSuite* is a set of graphical tools to quick visualize and 
explore/analyze monodimentional and bidimensional data. The plot 
created can be saved in a variety of image formats to produce 
publication quality graphs.

At the moment the project is composed by two little stand alone 
applications: *ImageAnalyzer* and *Plotfile2*. ImageAnalyzer is 
focused towards images (or any 2D data, such as arrays) 
visualization, analysis, and measurement. Plotfile2 focus is towards 
simple data series visualization, with the possibility to quick 
modify many plot characteristics. Both applications can save the 
result on a multitude of image file formats (png, eps, jpeg, pdf, and 
more).
"""

setup(name='PyPlotSuite',
      version=read_version('pyplotsuite/'),
      description='GUI tools for data plotting and analysis.',
      long_description=LONG_DESCRIPTION,
      author='Antonino Ingargiola',
      author_email='tritemio@gmail.com',
      url='http://pyplotsuite.sourceforge.net/',
      license='GPL',
      scripts=['./imageanalyzer', './plotfile2', 
          './imageanalyzer.pyw', './plotfile2.pyw'],
      packages=['pyplotsuite', 'pyplotsuite.imageanalyzer',
          'pyplotsuite.plotfile', 'pyplotsuite.common'],
      package_dir={'pyplotsuite': 'pyplotsuite'},
      package_data={
          '': ['README*', 'LICENSE.txt', 'Changelog*', ],
          'pyplotsuite': ['VERSION'],
          'pyplotsuite.imageanalyzer': ['*.glade', 'logo.png', 'icons/*',
              'samples/*'],
          'pyplotsuite.plotfile': ['*.glade', 'samples/*']},
      #data_files=[('bitmaps', ['bm/b1.gif', 'bm/b2.gif'])],
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: GNU General Public License (GPL)',
          'Intended Audience :: Science/Research',
          'Operating System :: OS Independent (Written in an interpreted language)',
          'Programming Language :: Python',
          'Topic :: Visualization',
          'User Interface :: GTK+',
          ],)
