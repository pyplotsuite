PyPlotSuite README File
=======================

.Copyright and License
*************************************************
Copyright (C) 2006-2007 Antonino Ingargiola <tritemio@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

For more information read the file LICENSE.txt in the source directory.
*************************************************


Project Description
-------------------

*PyPlotSuite* is a set of graphical tools to quick visualize and explore/analyze
monodimentional and bi-dimensional data. The plot created can be saved in a
variety of image formats to produce publication quality graphs.

At the moment the project is composed by two little stand alone applications:
*ImageAnalyzer* and *Plotfile2*. ImageAnalyzer is focused towards images (or any
2D data, such as arrays) visualization, analysis, and measurement. Plotfile2
focus is towards simple data series visualization, with the possibility to
quick modify many plot characteristics. Both applications can save the result
on a multitude of image file formats (png, eps, jpeg, pdf, and more).

PyPlotSuite is written in python, using the GTK gui toolkit (through the python
binding pygtk, and using Glade-2 to layout the windows). All plots are created
with the great Matplotlib python 2D graphic library. Matplotlib is such a good
tool that the PyPlotSuite applications are only tiny wrapper around Matplotlib
functionality. Also some Numpy functionality are used both through Matplotlib
(which requires Numpy) and both directly.

Home Page::         http://pyplotsuite.sourceforge.net[]
*Latest Version*::  0.1-alpha11-dev


NEWS 
----
0.1-alpha11-dev
~~~~~~~~~~~~~~~

Some ImageAnalyzer improovements:

* Added buttons to navigate through the images in the current dir
* Min and Max range are applied immediately, Apply button removed


0.1-alpha10: Bugfix release
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Fixed some small bugs presents in alpha9

* Show absolute path in "Image Properties"
* A trivial numarray.ndimage fix
* "Add by" and "Multiply by" little improvements


0.1-alpha9
~~~~~~~~~~
* New "Sum by" and "Multiply by" functions added.
* New Image Properties dialog which shows file name, size, max and min values 
  and coordinates, and other values (mean, variance, median, ...)
* Histogram plot speedups (thanks to the use of ndimage.measurements) and added
  selectable number of bins.
* Added tools to graphically set parameters to calculate integral of a 
  peak in the image. The integral domain is selected by the pixels inside a 
  circle that are above a threshold value. The circle center is selectable via 
  mouse click on the image. The pixels outside the circle that are below the 
  threshold are used to calculate the offset of the image. The results can be 
  saved to a text file.
* Added "Save file" to save modified images as arrays in pickle format.
* Added a virtual binning feature (similar of the binning feature of some 
  CCD). It reduces the image resolution building each pixel as the sum of the 
  values of the pixel in a rectangle. Note that this is not a boxcar filter 
  (and IIRC is not even a linear filter).
* Another bugfix from David Trémouilles (about dialog close button work on Mac)
  Thanks ;).


0.1-alpha8
~~~~~~~~~~
* Added multiplatform installer using distutils. Now to install the pyplotsuite
  system-wide just do "python setup.py install" form the source tree with
  administrative privileges.
* Various bug-fixes thanks to David Trémouilles' bug reports and patches (now 
  the open dialog remembers the last dir).
* scipy.filters is now only optional to run ImageAnalyzer
* ImageAnalyzer: Allow to change aspect ratio for images in ImageAnalyzer (via 
  the pixel dimension dialog)
* ImageAnalyzer: Zemax file format loading fixes and autodetection improvements.


0.1-alpha7
~~~~~~~~~~
Only Plotfile2 changes in this release. Various speed-up and code optimization, 
and the support for changing also the color for all line-plot elements.

0.1-alpha6
~~~~~~~~~~
This is the first version of PyPlotSuite released as a single tarball. It 
includes both ImageAnalyzer and Plotfile2.

A new plot property dialog in Plotfile2 allows to change many plot parameters 
(line style and width, marker style and size). There's also the GUI for 
changing the colors of the various plot elements but this functions is not 
implemented in the underlying code yet.


Release Notice
--------------

Early versions of *ImageAnalyzer* and *Plotfile2* were released in separated
tarballs with separated version number. Now they are released together in the
same tarball with the same version number (and I plan to integrate them more).
However they retain separated changelogs, so you can keep track of each
application separately.

Installation
------------
You can decompress the archive where you want and then execute PyPloSuite in 
place, entering the directory and typing:

--------
  python imageanalyzer
--------

or

--------
  python plotfile2
--------

You can install PyPlotSuite system wide with this command:

--------
  python setup.py install
--------


Development Version
-------------------

The development version of *PyPlotSuite* is hosted on a
http://repo.or.cz/w/pyplotsuite.git[git repository]. To download the latest
development version just install http://git.or.cz/[git] 
and type:

-------------------
  $ git clone git://repo.or.cz/pyplotsuite.git
-------------------

Now you have a complete working tree of PyPlotSuite to test/hack on. If git
does not work on your OS (I think only windows is affected) look at
http://bazaar-vcs.org/[bazar] that is multiplatform SCM with a git plugin too.


Requirements
------------

You must have this software installed in order to be able to run the program:

- Any recent version of Linux, Mac OSX or Windows

- Python 2.4 or newer (2.3 may still work, not tested):
    http://www.python.org[]

- PyGTK 2.x:
    http://www.pygtk.org/[]

- NumPy (1.0 or newer)
    http://numpy.scipy.org/[]

- MatPlotLib (0.87 or newer):
    http://matplotlib.sourceforge.net/[]

- SciPy (only the module scipy.ndimage.filters):
    http://www.scipy.org/[]

- Python Image Library (PIL):
    http://www.pythonware.com/products/pil/[]

NOTE: Typically Numpy is a prerequisite for Matplotlib, so you may have
installed it in bundle with Matplotlib.

NOTE: The Scipy module and the PIL library are a requirement only if you want
to run ImageAnalyzer. Plotfile2 does not requires them.


Image Analyzer
--------------

Name::              Image Analyzer
Description::       A tool to extract and make measures on scientific images.


Description
~~~~~~~~~~~

ImageAnalyzer is a python[1] program written with the purpose to analyze
scientific images to extract information such dimensions, and to reveal some
subtle features. You can also create and save publication quality figures in a
variety of hard-copy formats (using Matplotlib[5]).

To execute the program type:

-------------
  python imageanalyzer
-------------

Remember to check the software requirement above.


Image formats supported
~~~~~~~~~~~~~~~~~~~~~~~

All format supported by the PIL[6] library, with the following pixel formats:
- L (Luminance, aka black&white) 8 bit and 14 bit
- RBG(A) with 8bit per channel

Also 2D arrays saved in python pickle format can be loaded and saved.


Features
~~~~~~~~

Some features currently implemented are:

    - Load, zoom and navigate on any RBG(A) or L (Luminance, aka black&white)
      images.
    - The possibility to choose one of the following 17 interpolation 
      algorithms:
          Bessel, Bicubic, Bilinear, Blackman, Catrom, Gaussian, Hamming,
          Hanning, Hermite, Kaiser, Lanczos, Mitchell, Nearest, Quadric, Sinc,
          Spline16, Spline36.
      The default one is Nearest which is "no interpolation".
    - Select one of the 14 color-maps (only for L images).
    - Select a dynamic range either numerically and graphically through an
      histogram of the image (only for L images).
    - Grid on/off, black/white (to match various color-maps).
    - Calculate the distance between two points with the option to set the pixel
      dimension for the image (x and y separately).
    - Plot the pixel-precise profile of an arbitrary number of sections of the
      image (only for L images).
    - Gaussian filter the image setting different parameters for the X and 
      the Y direction.
    - Virtual binning of the image (as the binning feature of the CCD). An 
      arbitrary rectangular base-block size can be chosen.
    - Calculate the integral of a peak present in the image selecting the 
      integration domain using a threshold value (all the pixel with value 
      greater than the threshold are summed) and circular domain (outside 
      which no pixel is summed even is they are above the threshold). The 
      center of the circle can be also selected clicking on the image.
    - Add or multiply the image by a constant value.


ImageAnalyzer TODO
~~~~~~~~~~~~~~~~~~

To-do list in no particular order:

    - Allow to save modified images. *[DONE in pickle format]*
    - Allow to save/load plot properties (colormap, interpolation, zoom). 
      Handle this as explained in Plotfile2 TODO (below).
    - Allow to choose font size for title and labels (separately).
    - Fix axis aspect ratio in histogram window.
    - Better handling of failure on file load. *[partially done]*
    - Use Plotfile2 (instead of the current custom window) to show image 
      sections.
    - More image filtering options. *[partially done]*
    - Add pixel-value picker tool.
    - Peak integration tool (2d integration on a circular area). *[DONE]*
    - Noise removal tools (dithering, noise shaping, 2D matched filter, you
      name it! :D)


Plotfile2
---------

Name::              Plotfile2
Description::       A tool plot 1D data series.


Description
~~~~~~~~~~~

Plotfile2 is is a python[1] program written with the purpose to quick plot data
series stored in standard ascii text files. Multiple data series can be plotted,
compared and the resulting plot can be saved in a variety of image file formats
with a customizable size and resolution. This feature come particularly handy
if you want to embed your plot in a document and you what size the final image
will have.

Plotfile2 is written using the GTK toolkit[2] and embeds Matplotlib[5] plot
facility.

To execute the program, enter the Plotfile2 dir and type:

--------
  python plotfile2
---------

See also the software requirements above.


Features
~~~~~~~~

At the moment Plotfile2 can:

    - Load one or more data series from a two column (spaces or tab separated) 
      ascii file. Blank lines (only spaces or tabs) are ignored.
    - Detect if there is an header to automatically set axis labels. The header
      must be one line with two strings separated by spacing (or tabs). Blank
      lines before and after the header are ignored. More than two 
      space-separated string results in an error.
    - Controls to change the X and Y axis range, scale (linear or 
      log) and  toggle grid.
    - Controls to toggle if plot shows points (markers), lines or both. 
    - Complete customization of plot elements through a dedicated dialog
      (line style, marker type, colors, etc...).
    - Set the plot title and axis labels
    - Set the figure size (both in inches or in cm) and resolution (in dpi).
      This is useful to export a plot to be included in a document with a know
      size.
    - Navigate/explore plots (zoom, un-zoom, pan, etc) thanks to the Matplotlib
      NavigationToolbar2 (try to click on the crossed arrows icon and then 
      click and dragg with the right mouse button).
    - Save the plot in a multitude of image formats (png, jpeg, eps, pdf, ...
      all the formats supported by Matplotlib). Just click on the floppy icon.


Plotfile2 TODO
~~~~~~~~~~~~~~

- Possibility to customize plot line style, line color, line width, marker
  color, marker size, marker type for each series. *[COMPLETED]*
- Possibility to use spline interpolation (with various orders), and
  exponential-spline interpolation '[Work in progress]'.
- Add dialogs to choose an arbitrary color for plot elements in 
  the "Plot Properties" dialog.
- Allow font size (and if possible) font style change for labels and title.
- Save plots as data (two columns ascii file or csv). This is useful for
  new data generated by the interpolation.
- Allow to save and load plot properties associated with a set of data. This 
  would allow to load a dataset with associated plot style. To maintain the 
  data in a compatible format this metadata will be saved in a new file with 
  same name but different extension (f.e. ".plot"). Maybe the file should 
  be hidden... (in the same dir of the data of course).
- Add basic operators (spectrum, integrate, differentiate: which make sense?).
- Embed an (optional) [i]python shell.


Plotfile2 Known Problems
~~~~~~~~~~~~~~~~~~~~~~~~
- When you change figure size and resolution using the dedicated dialog, the
  main window may show a scrambled plot (or only a corner of the entire plot).
  This is only a visualization issue. Therefore, if you save the file (without
  resizing the window!) you will obtain the image file with the correct size
  and resolution ready to be included in your documents.

NOTE: If you have any suggestion to solve this problem please contact 
mailto:tritemio@gmail.com[me] via mail.


Links
-----

1. *Python*: a dynamic object-oriented programming language.

    * http://www.python.org[]


2. *PyGTK*: Python binding for the GTK user interface library.

    * http://www.pygtk.org/[]


3. *Numpy*: The fundamental package needed for numerical computing with Python.
    It is the successor (and unifier) of all the old projects (Numeric and
    Numarray) for python numerical computing.

    * http://numpy.scipy.org/[]


4. *SciPy*: Many modules that were not specific to Numpy are gathered under the
    broader project SciPy:

    * http://numpy.scipy.org/[]


5. *MatPlotLib*: A python 2D plotting library which produces publication quality
    figures in a variety of hard-copy formats and interactive environments
    across platforms.

    * http://matplotlib.sourceforge.net/[]


6. *Python Imaging Library* (PIL).

    * http://www.pythonware.com/products/pil/[]



***********************
The html version of this README was generated from the ASCII file with:

---------
$ asciidoc -a toc -a icons -a badges README.txt
---------
***********************

