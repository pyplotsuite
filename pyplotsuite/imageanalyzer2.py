#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Image Analizer -- A tool to visualize and analyze images
#   Version: 0.1-alpha (see VERSION file)
#   This program is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

import sys
import os
import os.path
import glob
import imp
rootdir =  os.path.abspath(imp.find_module('pyplotsuite')[1]) + '/'
sourcedir = rootdir + 'imageanalyzer/'
gladefile = sourcedir + 'image_analyzer.glade'

dark_suffix = '-buio'
debug = True

try:
    from PIL import Image
except ImportError:
    print '\n You need to install the Python Imaging Library (PIL) module. \n'
    sys.exit(3)

# Generic window description classes
from pyplotsuite.common.gtk_generic_windows import \
        GenericMainPlotWindow,  MyNavigationToolbar, GenericSecondaryWindow, \
        DialogWindowWithCancel, GenericSaveFileDialog, ChildWindow

# Dialogs classes
from imageanalyzer.histogramdialog import HistogramApp
from imageanalyzer.sectionsdialog import SectionApp

# Mathematical functions
from numpy import arange, array, sqrt, log10, \
        logical_and, logical_or, logical_not

# Search the optional module 'filters'
has_filters = True
try:
    import scipy.ndimage.filters as filters
    if debug: print 'Found scipy.ndimage.filters'
except ImportError:
    try:
        import numarray.nd_image.filters as filters
        if debug: print 'Found numarray.nd_image.filters'
    except ImportError:
        if debug: print 'WARNING: No filters module found, filters disabled.'
        has_filters = False

# Search the optional module 'measurements'
has_measurements = True
try:
    import scipy.ndimage.measurements as M
    print 'Found scipy.ndimage'
except ImportError:
    try:
        import numarray.nd_image.measurements as M
        print 'Found numarray.nd_image'
    except ImportError:
        print 'WARNING: No ndimage module found, some features disabled.'
        has_measurements = False

# Import matplotlib widgets and functions
from matplotlib.cm import get_cmap
from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter

# Some custom utility modules
from pyplotsuite.common.arrayfromfile import arrayfromfile, arrayFromZemax
from pyplotsuite.common.img import image2array
from pyplotsuite.common.geometry import circle
from pyplotsuite.common.version import read_version
from imageanalyzer.circularlist import CircularList
from imageanalyzer.section import ArraySection
from imageanalyzer.loadfile import *
from imageanalyzer.resampling import digital_binning
from imageanalyzer.integral import peak_integral1, peak_integral2t

# Import the profiler
#import profile
#profile.Profile.bias = 5e-6

def dprint(*args):
    global debug
    if debug: print args

##
# Main Window Class
#
class ImageApp(GenericMainPlotWindow):
    """
    Main application window object.
    """
    def __init__(self, filename=None):
        GenericMainPlotWindow.__init__(self, 'ImageWindow', gladefile,
                autoconnect=True, makeAxis=False, makeToolbar=False)

        self.image_loaded = False   # Must stay before gui_setup()
        self.gui_setup()
        self.filename = filename

        # Create additional attributes with default values
        self.gridon = True
        self.origin = 'lower'           # Image origin ('upper' or 'lower')
        self.gridcolor = 'white'
        self.xPixelDim = 1
        self.yPixelDim = 1
        self.min = 0
        self.max = 2**14+1
        self.interp = 'Nearest'         # corresponds to .set_active(12)
        self.colormap = get_cmap('jet') # corresponds to .set_active(8)
        #self.position = None
        self.histogramApp = None
        self.sectionApp = None
        self.colors = ('green', 'red', 'blue', 'magenta', 'cyan', 'yellow',
                'orange', 'gray')

        self.segments = []
        self.linescolors = []
        self.measuring = False      # - indicate if the measure button is pushed
        self.secondPoint = False    # - indicate if we're waiting for the second
                                    #   point of a distance measure
        self.title = None
        self.xlabel = None
        self.ylabel = None
        self.colorlist = None
        self.sectionsLC = None      # LinesCollection representing the sections
        self.aspect = 1
        self.PeakIntegralDialog = None

        # Show the window and each child
        self.window.show_all()
        
        self.load_files(filename)
    
    def generate_file_list(self, current_path=None):
        if current_path is None: current_path = '.'
        print 'current_path: ', current_path
        
        extensions = ['jpg', 'jpeg', 'png', 'tif', 'tiff']
        file_list = []
        for e in extensions:
            for ee in [e.lower(), e.upper(), e.title()]:
                file_list += glob.glob(current_path+'/*.'+ee)
        
        print 'file_list: ', file_list
        return file_list


    def load_files(self, filename, current_path=None):
        self.file_list = self.generate_file_list(current_path)

        if len(self.file_list) == 0 and filename is None:
            self.statusBar.push(self.context,
                    'Use menu File -> Open to load an image.')
            return

        self.file_index = 0
        if filename in self.file_list:
            self.file_index = self.file_list.index(filename)
        elif filename is not None:
            self.file_list = [filename] + self.file_list

        self.load_image(self.file_list[self.file_index])


    def gui_setup(self):
        # Create the matplotlib toolbar
        self.mpl_toolbar = MyNavigationToolbar(self.canvas, self.window,
                ButtonCallBack = self.on_measureButton_toggled,
                icon_fname = sourcedir+'icons/measure-icon-20.png',
                label = 'Measure Tool',
                tooltip = 'Enable/Disable the "Measure Tool".'
                )
        toolbar_container = self.widgetTree.get_widget('MyToolbarAlignment')
        toolbar_container.add(self.mpl_toolbar)

        # Create an handle for the grid check box
        self.gridCheckBox = self.widgetTree.get_widget('grid')

        # Create handles for the SpinBoxes
        self.minSpinButton = self.widgetTree.get_widget('minSpinButton')
        self.maxSpinButton = self.widgetTree.get_widget('maxSpinButton')

        # Set the handler for the colormap ComboBox
        self.cmapComboBox = self.widgetTree.get_widget('cmapComboBox')
        self.cmapComboBox.set_active(8)     # 'jet'

        # Set the handler for the interpolation ComboBox
        self.interpComboBox = self.widgetTree.get_widget('interpComboBox')
        self.interpComboBox.set_active(12)  # 'Nearest'

        self.openfileDialog = None
        self.savefileDialog = None

    
    ##
    # The following are plotting methods
    #
    def _load_file(self, fname):
        """
        Low-level function used by load_image to handle file open, format
        autodetection and loading of basic data.

        load_image requires that _load_file set the following:
            - self.imagemode
            - self.image_array
        and optionally also self.image (the PIL image object).
        """
        try:
            self.image_array, self.imagemode, self.image = load_file(fname)
        except IOError, error_string:
            self.statusBar.push(self.context, error_string)
            return False
        except UnknownFileType:
            self.statusBar.push(self.context,
                    'File "%s" is of unknown type.' % fname)
            return False
        else:
            return True

    def load_image(self, filename):
        """
        Load the given image filename, and plot it. It set also the statical
        attributes for a given image (.min, .max, .bits, .isColorImage)
        """

        # Try to open the file and load the image ...
        if not self._load_file(filename): return

        # ... and if the image is loaded ...
        self.window.set_title(os.path.basename(filename))
        self.figure.clf()
        self.axim = self.figure.add_subplot(111)

        if self.imagemode == 'L':
            self.nbit = 8
            self.isColorImage = False
        elif self.imagemode == 'I;16':
            self.nbit = 14
            self.isColorImage = False
        elif self.imagemode == 'floatarray':
            self.nbit = 8
            self.isColorImage = False
        else:
            self.nbit = 8
            self.isColorImage = True

        if self.isColorImage:
            self.statusBar.push(self.context,
                    'Image RGB(A): ignoring the range (min and max).')
        else:
            self.min = self.image_array.min()
            self.max = self.image_array.max()
            if self.imagemode == 'floatarray':
                s = 'float values.'
            else:
                s = str(self.nbit)+' bit.'
            self.statusBar.push(self.context,
                    'Image L (Luminance) with '+s)

        self.autoset_extent()
        self.plot_image(self.min, self.max)
        self.image_loaded = True

    def plot_image(self, min=None, max=None, interp=None, cmap_name=None,
            origin=None, extent=None):
        """
        Plot the image self.image_array. The optional parameters allow to
        choose min and max range value, interpolation method and colormap.
        If not specified the default values are took from the object
        attributes (self.min, self.max, self.cmap, self.interp).

        Assumes the statical attributes for the given image to be set (for
        example .isColorImage).
        """
        if not min == None: self.set_min(min)
        if not max == None: self.set_max(max)
        if not interp == None: self.interp = interp
        if not origin == None: self.origin = origin
        if not cmap_name == None: self.colormap = get_cmap(cmap_name)
        if not extent == None: self.extent = extend

        self.axim.clear()
        
        self.i = self.axim.imshow(self.image_array,
                interpolation=self.interp,
                vmin=self.min, vmax=self.max,
                cmap=self.colormap,
                origin=self.origin,
                extent=self.extent,
                aspect=self.aspect)

        if not self.isColorImage:
            # Plot the colorbar
            try:
                # Try to plot the colorbar on an existing second axis
                self.figure.colorbar(self.i, self.figure.axes[1])
            except:
                # Otherwise let colorbar() to create its default axis
                self.figure.colorbar(self.i)

        if self.gridon:
            self.axim.grid(color=self.gridcolor)
        # self.axim.axis('image')
        self.set_title_and_labels()
        if self.sectionsLC != None:
            self.axim.add_collection(self.sectionsLC)
        self.canvas.draw()

    def autoset_extent(self):
        """
        Auto-set the image extent using image dimension and pixel size.
        """
        if self.isColorImage:
            ly, lx, ch = self.image_array.shape
        else:
            ly, lx = self.image_array.shape
        self.extent = (0, lx*self.xPixelDim, 0, ly*self.yPixelDim)

    def set_min(self, min):
        self.min = min
        self.minSpinButton.set_value(min)

    def set_max(self, max):
        self.max = max
        self.maxSpinButton.set_value(max)

    def set_title_and_labels(self):
        if self.title != None:
            self.axim.set_title(self.title)
        if self.xlabel != None:
            self.axim.set_xlabel(self.xlabel)
        if self.ylabel != None:
            self.axim.set_ylabel(self.ylabel)

    ##
    # Methods for the "Measure" tool and to manage sections
    #
    def on_canvas_clicked(self, event):
        if not event.inaxes == self.axim: return
        if not self.secondPoint:
            self.secondPoint = True
            print "x:", event.xdata, ", y:", event.ydata
            self.x1 = int(event.xdata) + 0.5
            self.y1 = int(event.ydata) + 0.5
            m =  'First point: X1 = '+str(self.x1)+', Y1 = '+str(self.y1)+'.'
            m += ' Select the second point.'
            self.statusBar.push(self.context, m)
        else:
            print "x:", event.xdata, ", y:", event.ydata
            x2 = int(event.xdata) + 0.5
            y2 = int(event.ydata) + 0.5

            self.segments.append(((self.x1, self.y1), (x2, y2)))
            self.linescolors.append(self.colorlist.next())
            self.sectionsLC = LineCollection(self.segments,
                    colors=self.linescolors)
            self.sectionsLC.set_linewidth(3)
            self.axim.add_collection(self.sectionsLC)
            self.canvas.draw()

            # Print distance to status bar
            self.calculateDistance(self.segments[-1])

            if self.sectionApp != None:
                self.sectionApp.addsection(self.calculateSection(-1))

            self.secondPoint = False
            self.x1, self.y1 = None, None

    def calculateDistance(self, section, useStatusBar=True):
        (x1, y1), (x2, y2) = section
        dx = (x2 - x1)
        dy = (y2 - y1)
        dist = sqrt(dx**2 + dy**2)
        if useStatusBar:
            m =  'X1 = %d, Y1 = %d; ' % (self.x1, self.y1)
            m += 'X2 = %d, Y2 = %d; ' % (x2, y2)
            m += 'Distance: %5.1f μm.' % (dist)
            self.statusBar.push(self.context, m)
        return dist

    def calculateSection(self, segmentindex):
        (x1, y1), (x2, y2) = self.segments[segmentindex]

        print 'xpdim:', self.xPixelDim, 'ypdim:', self.yPixelDim
        px = lambda xi: int(round(xi/float(self.xPixelDim)))
        py = lambda yi: int(round(yi/float(self.yPixelDim)))

        px1, px2, py1, py2 = px(x1), px(x2), py(y1), py(y2)
        print 'Segment:', px1, px2, py1, py2

        if self.origin == 'upper':
            py1, py2 = self.image_array.shape[0] - array((py1, py2)) - 1
            print 'Segment UpDown:', px1, px2, py1, py2

        section = ArraySection(self.image_array, px1, px2, py1, py2,
                self.xPixelDim, self.yPixelDim, debug=True)

        # Recall the corresponding section color
        section.color = self.linescolors[segmentindex]

        return section

    ##
    # The following are Toolbar callbacks
    #
    def on_cmapComboBox_changed(self, widget, *args):
        colormap_name = self.cmapComboBox.get_active_text().lower()
        self.colormap = get_cmap(colormap_name)
        if not self.image_loaded: return
        elif self.isColorImage:
            self.colormap = get_cmap(colormap_name)
            self.statusBar.push(self.context,
                    'Colormaps are ignored for color images.')
        else:
            self.i.set_cmap(self.colormap)
            self.canvas.draw()

    def on_interpComboBox_changed(self, widget, *args):
        if not self.image_loaded: return None
        interpolation_mode = self.interpComboBox.get_active_text()
        dprint ("Interpolation:", interpolation_mode)
        self.interp = interpolation_mode
        self.i.set_interpolation(interpolation_mode)
        self.canvas.draw()

    def on_histogramButton_clicked(self, widget, *args):
        if not self.image_loaded:
            self.statusBar.push(self.context,
                    'You should open an image to visualyze the histogram.')
            return None
        elif self.isColorImage:
            self.statusBar.push(self.context,
                    'Histogram view is not available for color images.')
            return None

        if self.histogramApp == None:
            dprint ("new histogram")
            self.histogramApp = HistogramApp(gladefile, self, self.nbit,
                    debug=debug)
        else:
            dprint ("old histogram")
            self.histogramApp.window.show()

    def on_maxSpinButton_value_changed(self, widget, *args):
        max = self.maxSpinButton.get_value()
        if self.min >= max:
            self.maxSpinButton.set_value(self.max)
            self.writeStatusBar("Invalid range value: must be Min < Max.")
        elif self.max != max:
            self.max = max
            self.plot_image(self.min, self.max)
            self.writeStatusBar("New range applied.")

    def on_minSpinButton_value_changed(self, widget, *args):
        min = self.minSpinButton.get_value()
        if self.max <= min:
            self.minSpinButton.set_value(self.min)
            self.writeStatusBar("Invalid range value: must be Min < Max.")
        elif self.min != min:
            self.min = min
            self.plot_image(self.min, self.max)
            self.writeStatusBar("New range applied.")
    
    def on_prevImageButton_clicked(self, widget, *args):
        print 'prev'

        # if we are at the beginning of the list do nothing
        if self.file_index == 0: return
        
        if self.file_index == -1:
            # if the file loaded was not on the list load the first
            self.file_index = 0
        else:
            self.file_index -=1

        self.load_image(self.file_list[self.file_index])

    
    def on_nextImageButton_clicked(self, widget, *args):
        print 'next'
        
        # if we are at the end of the list do nothing
        if self.file_index == len(self.file_list) - 1: return
        
        if self.file_index == -1: 
            # if the file loaded was not on the list load the first
            self.file_index = 0
        else:
            self.file_index +=1

        self.load_image(self.file_list[self.file_index])

    
    def on_applyButton_clicked(self, widget, *args):
        min = self.minSpinButton.get_value()
        max = self.maxSpinButton.get_value()
        if min >= max:
            self.minSpinButton.set_value(self.min)
            self.maxSpinButton.set_value(self.max)
            self.writeStatusBar("Invalid range value: must be Min < Max.")
        elif self.min != min or self.max != max:
            self.min, self.max = min, max
            self.plot_image(self.min, self.max)
            self.writeStatusBar("New range applied.")

    def on_measureButton_toggled(self, widget, data=None):
        self.measuring = not self.measuring
        if self.measuring:
            # Create the list of colors the first time
            if self.colorlist == None:
                self.colorlist = CircularList(len(self.colors))
                for c in self.colors:
                    self.colorlist.append(colorConverter.to_rgba(c))
            # Connect the cb to handle the measure
            self.cid = self.canvas.mpl_connect('button_release_event',
                    self.on_canvas_clicked)
            self.statusBar.push(self.context,
                    '"Measure Length" tool enabled: select the first point.')
        else:
            self.statusBar.push(self.context,
                    '"Measure Length" tool disabled.')
            self.canvas.mpl_disconnect(self.cid)

    ##
    # The following are menu callbacks
    #
    
    ## File
    def on_openfile_activate(self, widget, *args):
        if self.openfileDialog is None:
            self.openfileDialog = OpenFileDialog(self)
        else:
            self.openfileDialog.window.show()
    
    def on_savefile_activate(self, widget, *args):
        if self.savefileDialog is None:
            self.savefileDialog = SaveFileDialog(self)
        else:
            self.savefileDialog.window.show()

    def on_reload_activate(self, widget, *args):
        dprint ("Reloading image ...")
        if self.filename != None:
            self.on_clearsegments_activate(None, None)
            self.load_image(self.filename)
    
    def on_close_activate(self, widget, *args):
        self.figure.clf()
        self.canvas.draw()
        self.image_loaded = False

    def on_quit_activate(self, widget, *args):
        self.on_imagewindow_destroy(widget, *args)

    ## Modify
    def on_scrollingmean_activate(self, widget, *args):
        if not self.isColorImage:
            print 'Type:', self.image_array.dtype.name
            self.image_array = self.image_array.astype('float32')
            print  'Convolution spatial filter ... '
            kernel = array([[0, 1, 0], [1, 1, 1], [0, 1, 0]], dtype='float32')
            kernel /= kernel.sum()
            print '  - Convolution kernel: \n', kernel
            self.image_array = filters.convolve(self.image_array, kernel)
            self.imagemode = 'floatarray'
            self.plot_image()
            print 'OK\n'
        else:
            self.statusBar.push(self.context,
                    "This mean can't be performed on color images.")

    def on_binning_activate(self, widget, *args):
        BinningDialog(self)

    def on_gaussianFilter_activate(self, widget, *args):
        dprint ("Gaussian Filer")
        GaussianFilterWindow(self)

    def on_clearsegments_activate(self, widget, *args):
        if self.segments == []: return
        if self.sectionApp != None:
            self.sectionApp.destroy()
            self.sectionApp = None
        self.segments = []
        self.linescolors = []
        self.colorlist.resetIndex()
        self.sectionsLC =  None
        #self.axim.clear()
        self.plot_image()

    def on_resetrange_activate(self, widget, *args):
        self.plot_image(
                min=int(self.image_array.min()),
                max=int(self.image_array.max()))

    def on_pixelDimension_activate(self, widget, *args):
        PixelDimensionWindow(self)

    def on_title_and_axes_labels_activate(self, widget, *args):
        TitleAndAxesWindow(self)

    ## View
    def on_downup_activate(self, widget, *args):
        if self.origin == 'upper': self.origin = 'lower'
        else: self.origin = 'upper'
        if self.image_loaded: self.plot_image()
        if self.sectionApp != None:
            print " *** WARNING: swap of segments not implemented."

    def on_grid_activate(self, widget, data=None):
        if self.gridon: self.axim.grid(False)       # Toggle ON -> OFF
        else: self.axim.grid(color=self.gridcolor)  # Toggle OFF -> ON
        self.canvas.draw()
        self.gridon = not self.gridon

    def on_whitegrid_activate(self, widget, *args):
        if self.gridcolor == 'white': self.gridcolor = 'black'
        else: self.gridcolor = 'white'
        dprint ("grid color:", self.gridcolor)

        if self.gridon:
            self.axim.grid(self.gridon, color=self.gridcolor)
            self.canvas.draw()

    def on_sectionlist_activate(self, widget, *args):
        colors = CircularList(len(self.colors))
        colors.fromlist(self.colors)
        text = ''
        for n, seg in enumerate(self.segments):
            (x1, y1), (x2, y2) = seg
            lenAU = self.calculateDistance(seg, False)
            text += '== Section '+ str(n+1) +' ('+ colors.get(n)+') ==\n'
            text += 'x1 = %4d   y1 = %4d\n' % (x1, y1)
            text += 'x2 = %4d   y2 = %4d\n' % (x2, y2)
            text += 'Length: %5.2f, with pixel dimention (%2.2f x %2.2f).\n'\
                    % (lenAU, self.xPixelDim, self.yPixelDim)
            text += '\n'
        SectionListApp(text)

    def on_sections_activate(self, widget, *args):
        if self.isColorImage:
            self.statusBar.push(self.context,
                    'Function not available for color images.')
        elif self.segments == []:
            self.statusBar.push(self.context, 'No section to plot.')
        elif self.sectionApp == None:
            for index, color in enumerate(self.linescolors):
                dprint (index, color, "cycle")
                aSection = self.calculateSection(index)
                print 'color', aSection.color
                if self.sectionApp == None:
                    self.sectionApp = SectionApp(gladefile, aSection, self)
                else:
                    self.sectionApp.addsection(aSection)
        else:
            self.sectionApp.window.show()
    
    ## Calculate
    def on_peakintegral_activate(self, widget, *args):
        if self.PeakIntegralDialog is None:
            self.PeakIntegralDialog = PeakIntegralDialog(self)
        else:
            self.PeakIntegralDialog.window.show()

    def on_sum_by_activate(self, widget, *args):
        SumByDialog(self)

    def on_multiply_by_activate(self, widget, *args):
        MultiplyByDialog(self)

    def on_image_properties_activate(self, widget, *args):
        ImagePropertiesDialog(self)

    ## About
    def on_information_activate(self, widget, *args):
        about = AboutDialog()


##
# Dialogs Classes
#
class SaveFileDialog(GenericSaveFileDialog):
    """
    This class implements the "Save File" dialog for the main window.
    """
    def __init__(self, callerApp):
        GenericSaveFileDialog.__init__(self, 'savefileDialog',
                gladefile, callerApp)

    def saveToSelectedFile(self):
        filename = self.filename + '.pickle'
        try:
            f = open(filename, 'w')
            cPickle.dump(self.callerApp.image_array, f)
            f.close()
        except:
            self.callerApp.writeStatusBar("Cannot save to file '%s'." % filename)
        else:
            self.callerApp.writeStatusBar('File "%s" saved.' % (filename))
            self.callerApp.window.set_title(os.path.basename(filename))
            self.callerApp.filename = filename

        self.window.hide()

class OpenFileDialog(DialogWindowWithCancel):
    """
    This class implements the "Open File" dialog.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'openfileDialog',
                gladefile, callerApp)

    def openSelectedFile(self):
        filename = self.window.get_filename()
        if filename != None:
            self.window.hide()
            self.callerApp.load_files(filename, os.path.dirname(filename))
        self.window.hide()

    def on_openButton_clicked(self, widget, *args):
        dprint ("open button clicked", self.window.get_filename())
        self.openSelectedFile()

    def on_openfileDialog_file_activated(self, widget, *args):
        dprint ("Open File Dialog: file_activated", self.window.get_filename())

    def on_openfileDialog_response(self, widget, *args):
        dprint ("\nOpen File Dialog: response event")

    def on_cancelButton_clicked(self, widget, *args):
        # Don't destroy the dialog so we remember the folder next time
        self.window.hide()
        return True

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class GaussianFilterWindow(DialogWindowWithCancel):
    """
    Dialog for setting the gaussian filter options.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'GaussianFilterWindow',
                gladefile, callerApp)
        self.sigmaSpinButton = self.widgetTree.get_widget('sigmaSpinButton')

    def on_okButton_clicked(self, widget, *args):
        sigma = self.sigmaSpinButton.get_value()
        dprint ("Ok, sigma =", sigma)
        self.callerApp.image_array = array(filters.gaussian_filter(
                self.callerApp.image_array, sigma))
        self.callerApp.imagemode = 'floatarray'
        self.callerApp.plot_image()
        self.window.destroy()

class BinningDialog(DialogWindowWithCancel):
    """
    Dialog for setting the binning parameters.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'BinningDialog',
                gladefile, callerApp)

        self.xBinSpinButt = self.widgetTree.get_widget('xBinSpinButt')
        self.yBinSpinButt = self.widgetTree.get_widget('yBinSpinButt')

    def on_okButton_clicked(self, widget, *args):
        dprint ("OK")
        xb = int(self.xBinSpinButt.get_value())
        yb = int(self.yBinSpinButt.get_value())
        a = self.callerApp.image_array
        self.callerApp.image_array = digital_binning(a, xb, yb)
        self.callerApp.autoset_extent()
        a = self.callerApp.image_array
        self.callerApp.plot_image(min=int(a.min()), max=int(a.max()))
        self.window.destroy()

class PixelDimensionWindow(DialogWindowWithCancel):
    """
    Dialog for setting the pixel dimension.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'PixelDimensionWindow',
                gladefile, callerApp)

        xDim = self.callerApp.xPixelDim
        yDim = self.callerApp.yPixelDim

        self.xPixDimSpinButt = self.widgetTree.get_widget('xPixDimSpinButt')
        self.yPixDimSpinButt = self.widgetTree.get_widget('yPixDimSpinButt')
        self.aspectSpinButt = self.widgetTree.get_widget('aspectSpinButt')
        self.xPixDimSpinButt.set_value(xDim)
        self.yPixDimSpinButt.set_value(yDim)
        self.aspectSpinButt.set_value(self.callerApp.aspect)

    def on_okButton_clicked(self, widget, *args):
        dprint ("OK")
        self.callerApp.xPixelDim = self.xPixDimSpinButt.get_value()
        self.callerApp.yPixelDim = self.yPixDimSpinButt.get_value()
        self.callerApp.aspect = self.aspectSpinButt.get_value()
        self.callerApp.autoset_extent()
        self.callerApp.plot_image()
        self.window.destroy()

class TitleAndAxesWindow(DialogWindowWithCancel):
    """
    Dialog for setting Title and axes labels.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'TitleAndAxesWindow',
                gladefile, callerApp)

        self.titleEntry = self.widgetTree.get_widget('titleEntry')
        self.xlabelEntry = self.widgetTree.get_widget('xlabelEntry')
        self.ylabelEntry = self.widgetTree.get_widget('ylabelEntry')

        def sync(field, entry):
            if field != None: entry.set_text(field)

        sync(self.callerApp.title, self.titleEntry)
        sync(self.callerApp.xlabel, self.xlabelEntry)
        sync(self.callerApp.ylabel, self.ylabelEntry)

    def on_okButton_clicked(self, widget, *args):
        dprint ("Ok clicked")
        self.callerApp.axim.set_title(self.titleEntry.get_text())
        self.callerApp.axim.set_xlabel(self.xlabelEntry.get_text())
        self.callerApp.axim.set_ylabel(self.ylabelEntry.get_text())
        self.callerApp.canvas.draw()

        self.callerApp.title = self.titleEntry.get_text()
        self.callerApp.xlabel = self.xlabelEntry.get_text()
        self.callerApp.ylabel = self.ylabelEntry.get_text()

        self.window.destroy()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class SectionListApp(GenericSecondaryWindow):
    """
    This class implement the text window used to show the sections' list.
    """
    def __init__(self, text):
        GenericSecondaryWindow.__init__(self, 'SectionListWindow', gladefile)

        textview = self.widgetTree.get_widget('sectionsTextView')
        buffer = textview.get_buffer()
        buffer.set_text(text)

    def on_closeButton_clicked(self, widget, data=None):
        self.window.destroy()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class PeakIntegralDialog(DialogWindowWithCancel):
    """
    Dialog for calculating the integral of a peak.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'PeakIntegralDialog',
                gladefile, callerApp)

        self.contour = None
        self.circle = None
        self.allowReplot = False
        self.yc, self.xc = None, None
        self.t = self.rt = None
        self.OutputDialog = None
        self.cid = None

        self.image = self.callerApp.image_array
        self.ax = self.callerApp.axim
        self.canvas = self.callerApp.canvas
        self.origin = self.callerApp.origin
        
        self.gui_setup()
        self.allowReplot = True
        self.update_all()
       
    def gui_setup(self):
        ## Load the GUI widgets
        widgets = ['thresholdSpinButt',  'rThresholdSpinButt', 
                'maxRadiusSpinButt', 'xCircleSpinButt', 'yCircleSpinButt',
                'circleFrame', 'thresholdFrame', 
                'circleCheckButt', 'thresholdCheckButt']

        for w_name in widgets:
            setattr(self, w_name, self.widgetTree.get_widget(w_name))
            if getattr(self, w_name) is None: 
                print " *** ERROR: No GUI object found for '%s'" % w_name
        
        ## Connect the click event to select the circle center
        self.connect_mouse()

        ## Set default values
        t = (self.image.max() - self.image.min())*0.1 + self.image.min()
        self.thresholdSpinButt.set_value(t)

        r = self.image.shape[0] / 8
        self.maxRadiusSpinButt.set_value(r)
        
        self.yc, self.xc = M.maximum_position(self.image)
        if self.origin == 'upper':
            dprint('upper origin')
            self.yc =  self.image.shape[0] - self.yc

        self.xCircleSpinButt.set_value(self.xc)
        self.yCircleSpinButt.set_value(self.yc)
        
        self.circleFrame.set_sensitive(self.circleCheckButt.get_active())
        self.thresholdFrame.set_sensitive(self.thresholdCheckButt.get_active())
    
    def calculate_integral(self):
        dprint('Calculating integral')
        a, t, rt, xc, yc, r = self.image, self.t, self.rt, self.xc, self.yc, \
                self.r

        if self.origin == 'upper':
            yc = self.image.shape[0] - yc

        if self.thresholdCheckButt.get_active():
            t, rt = 0., 0.
        
        if self.circleCheckButt.get_active():
            xc, yc, r = 0., 0., 0.
        
        raw_integral, domain_size, raw_residual, offset_integral, offset = \
                peak_integral2t(a, t, rt, xc, yc, r, 
                        self.circleCheckButt.get_active(),
                        self.thresholdCheckButt.get_active())
    
        data = "Raw Integral: \t %d \n" % (raw_integral)
        data += "Integrated pixels: \t %d \n" % (domain_size)
        data += "Raw Residual: \t %d \n" % (raw_residual)
        data += "Residual pixels: \t %d \n" % (a.size - domain_size)
        data += "Integral - offset: \t %1.2f \n" % (offset_integral)
        data += "Offset: \t\t %1.4f \n" % (offset)
        
        val = [t, rt, xc, yc, r]
        for i,v in enumerate(val):
            if v is None: val[i] = 0.
        data += "t, rt, xc, yc, r: \t %1.1f, %1.1f, %1.1f, %1.1f, %1.1f\n\n" \
                % tuple(val)
        
        if self.OutputDialog is None:
            self.OutputDialog = OutputDialog(data)
        else:
            self.OutputDialog.add_text(data)
            self.OutputDialog.window.show()
    ##
    # Plotting methods
    #
    def get_cursor_cb(self, event):
        if event.inaxes:
            dprint('setting new center')
            self.allowReplot = False
            self.xCircleSpinButt.set_value(event.xdata)
            self.allowReplot = True
            self.yCircleSpinButt.set_value(event.ydata)
            self.canvas.draw()

    def update_all(self):
        self.update_circle()
        self.update_mask()
        self.update_contour()
        self.canvas.draw()

    def update_mask(self):
        if self.thresholdCheckButt.get_active():
            self.t = self.thresholdSpinButt.get_value()
            self.rt = self.rThresholdSpinButt.get_value()
            
            if self.t > 0:
                mask = self.image > self.t
            else:
                mask = True
            
            if self.t > 0:
                rmask = self.image > self.rt
            else:
                rmask = True
            
            if self.circleCheckButt.get_active():
                r, xc, yc = [ getattr(self, a).get_value() for a in [
                    'maxRadiusSpinButt', 'xCircleSpinButt', 'yCircleSpinButt']]

                # Creating distance function using numpy broadcasting rules
                x = arange(self.image.shape[1])
                y = arange(self.image.shape[0]).reshape(self.image.shape[0],1)
                if self.origin == 'upper':
                    yc = self.image.shape[0] - yc
                distance = sqrt((x-xc)**2 + (y-yc)**2)
                self.mask = logical_or(logical_and(mask, distance < r),
                    logical_and(rmask, distance > r))
            else:
                self.mask = mask

    def update_contour(self):
        if self.thresholdCheckButt.get_active() and self.mask is not True:
            if self.contour is not None: 
                self.delete_contour()
            self.contour = self.ax.contourf(self.mask, [-1, 0.5], 
                    colors=['black'], alpha=0.7, origin=self.origin)
    
    def update_circle(self):
        if self.circleCheckButt.get_active():
            self.r = self.maxRadiusSpinButt.get_value()
            self.xc = self.xCircleSpinButt.get_value()
            self.yc = self.yCircleSpinButt.get_value()

            if self.allowReplot:
                x, y = circle(self.xc, self.yc, self.r)
               
                if self.circle is None:
                    self.circle, = self.ax.plot(x, y, 
                            lw=2, color='white', linestyle='--')
                else:
                    self.circle.set_data(x,y)

                self.ax.set_xlim(0,self.image.shape[1])
                self.ax.set_ylim(0,self.image.shape[0])

    def delete_contour(self):
        self.callerApp.plot_image()
        self.circle = None
        self.update_circle()
        self.contour = None
    
    def delete_circle(self):
        if self.circle is not None:
            self.circle.set_data([],[])

    def disconnect_mouse(self):
        if self.cid is not None:
            self.ax.figure.canvas.mpl_disconnect(self.cid)
            self.cid = None

    def connect_mouse(self):
        if self.cid is None:
            self.cid = self.ax.figure.canvas.mpl_connect(
                    'button_release_event', self.get_cursor_cb)
            self.callerApp.writeStatusBar(
                    "Click on the image to select the center of integration.")

    ##
    # GUI callbacks
    #
    def on_radiusCheckButt_toggled(self, widget, *args):
        dprint ("radius toggled")
        use_radius = widget.get_active()
        self.circleFrame.set_sensitive(use_radius)

        if use_radius:
            self.connect_mouse()
            self.update_circle()
            self.update_mask()
            self.update_contour()
        else:
            self.disconnect_mouse()
            self.delete_circle()
            self.update_mask()
            self.update_contour()
        self.canvas.draw()
    
    def on_thresholdCheckButt_toggled(self, widget, *args):
        dprint ("threshold toggled")
        use_threshold = widget.get_active()
        self.thresholdFrame.set_sensitive(use_threshold)
        
        if use_threshold:
            self.update_mask()
            self.update_contour()
        else:
            self.delete_contour()
        self.canvas.draw()
    
    def on_thresholdSpinButt_value_changed(self, widget, *args):
        dprint ("threshold value changed")
        if self.allowReplot: 
            self.update_mask()
            self.update_contour()
            self.canvas.draw()
    
    def on_rThresholdSpinButt_value_changed(self, widget, *args):
        dprint ("residual threshold value changed")
        if self.allowReplot: 
            self.update_mask()
            self.update_contour()
            self.canvas.draw()


    def on_maxRadiusSpinButt_value_changed(self, widget, *args):
        dprint ("radious value changed")
        if self.allowReplot: 
            self.update_mask()
            self.update_contour()
            self.update_circle()
            self.canvas.draw()

    def on_xCircleSpinButt_value_changed(self, widget, *args):
        dprint ("x coord value changed")
        if self.allowReplot: 
            self.update_mask()
            self.update_contour()
            self.update_circle()
            self.canvas.draw()
     
    def on_yCircleSpinButt_value_changed(self, widget, *args):
        dprint ("y coord value changed")
        if self.allowReplot: 
            self.update_mask()
            self.update_contour()
            self.update_circle()
            self.canvas.draw()
    
    def on_calculateButton_clicked(self, widget, *args):
        dprint ("Calculate")
        self.calculate_integral()

    def on_okButton_clicked(self, widget, *args):
        dprint ("OK")
        self.calculate_integral()
        self.callerApp.plot_image()
        self.window.destroy()
    
    def on_destroy(self, widget, *args):
        dprint ("destroing dialog")
        self.callerApp.plot_image()
        self.callerApp.PeakIntegralDialog = None
        self.window.destroy()

class ImagePropertiesDialog(ChildWindow):
    """
    Dialog to calculate image properties. 
    """
    def __init__(self, callerApp):
        ChildWindow.__init__(self, 'ImagePropertiesDialog', gladefile, 
                callerApp)
        
        self.widgetTree.get_widget('fnameEntry').set_text(
                os.path.basename(self.callerApp.filename))
        self.widgetTree.get_widget('pathEntry').set_text(
                os.path.abspath(os.path.dirname(self.callerApp.filename)))
        self.widgetTree.get_widget('sizeEntry').set_text("%3.1f KB" %\
                (os.stat(self.callerApp.filename).st_size/1024.,))

        ## Load the GUI widgets
        widgets = ['nPixelsEntry', 'shapeLabel', 'maxEntry', 'maxPosLabel',
            'minEntry', 'minPosLabel', 'varEntry', 'stdEntry', 'sumEntry',
            'meanEntry', 'medianEntry', 'modeEntry']

        for w_name in widgets:
            setattr(self, w_name, self.widgetTree.get_widget(w_name))
            if getattr(self, w_name) is None: 
                print " *** ERROR: No GUI object found for '%s'" % w_name
            
        im = self.callerApp.image_array
        self.nPixelsEntry.set_text(str(im.size))
        self.shapeLabel.set_text("%d x %d" % im.shape[::-1])
        self.maxEntry.set_text(str(im.max()))
        self.maxPosLabel.set_text("@ %d x %d" % M.maximum_position(im)[::-1])
        self.minEntry.set_text(str(im.min()))
        self.minPosLabel.set_text("@ %d x %d" % M.minimum_position(im)[::-1])
        self.varEntry.set_text("%1.3f" % im.var())
        self.stdEntry.set_text("%1.3f" % im.std())
        self.sumEntry.set_text("%d" % im.sum())
        self.meanEntry.set_text("%1.3f" % im.mean())
        self.medianEntry.set_text("%1.2f" %(im.min() + (im.max()-im.min())*0.5))
        self.modeEntry.set_text('Not yet implemented')
        self.modeEntry.set_sensitive(False)

    def on_closeButton_clicked(self, widget, *args):
        self.window.destroy()
    
class OperatorDialogSpinButt(DialogWindowWithCancel):
    """
    Generic class for a dialog with a spin button that applies an operator to 
    the image.
    """
    def __init__(self, name, SpinButtonName, gladefile, callerApp):
        ChildWindow.__init__(self, name,  gladefile, callerApp)
        self.SpinButt = self.widgetTree.get_widget(SpinButtonName)
        self.old_image = self.callerApp.image_array.copy()
        self.callerApp.image_array = self.callerApp.image_array.astype('float')

    def calculate(self):
        raise NotImplementedError
    
    def on_SpinButt_value_changed(self, widget, *args):
        dprint('SpinButt value changed')
        self.calculate()
        self.callerApp.plot_image()
    
    def on_applyButton_clicked(self, widget, *args):
        self.calculate()
        self.callerApp.plot_image()

    def on_okButton_clicked(self, widget, *args):
        self.calculate()
        self.callerApp.plot_image()
        self.window.destroy()

    def on_cancelButton_clicked(self, widget, *args):
        self.callerApp.image_array = self.old_image
        self.callerApp.plot_image()
        self.window.destroy()

class SumByDialog(OperatorDialogSpinButt):
    """
    Dialog to sum a value to the image. 
    """
    def __init__(self, callerApp):
         OperatorDialogSpinButt.__init__(self, 'SumByDialog', 'sumBySpinButt', 
                 gladefile, callerApp)

    def calculate(self):
        self.callerApp.image_array += self.SpinButt.get_value()

class MultiplyByDialog(OperatorDialogSpinButt):
    """ 
    Dialog to multiply a value to the image. 
    """
    def __init__(self, callerApp):
        OperatorDialogSpinButt.__init__(self, 'MultiplyByDialog',
                'multBySpinButt', gladefile, callerApp)
        
    def calculate(self):
        self.callerApp.image_array *= self.SpinButt.get_value()

class SaveOutputDialog(GenericSaveFileDialog):
    """
    This class implements a "Save File" dialog for the output of a dialog.
    """
    def __init__(self, data, callerApp=None):
        GenericSaveFileDialog.__init__(self, 'savefileDialog',
                gladefile, callerApp)
        self.data = data

    def saveToSelectedFile(self):
        filename = self.filename
        try:
            f = open(filename, 'w')
            f.write(self.data)
            f.close()
            #self.callerApp.writeStatusBar('File "%s" saved.' % (filename))
        except:
            #self.callerApp.writeStatusBar("Can't save file '%s'." % (filename))
            pass
        self.window.hide()

class OutputDialog(GenericSecondaryWindow):
    """
    This class implements a dialog to output some text.
    """
    def __init__(self, text):
        GenericSecondaryWindow.__init__(self, 'OutputDialog', gladefile)
        textview = self.widgetTree.get_widget('textview')
        self.buffer = textview.get_buffer()
        self.buffer.set_text(text)
        self.data = text
    
    def add_text(self, text):
        self.data += text
        self.buffer.set_text(self.data)
    
    def on_saveButton_clicked(self, widget, *args):
        dprint ("clear clicked")
        SaveOutputDialog(self.data)

    def on_clearButton_clicked(self, widget, *args):
        dprint ("save clicked")
        self.data=''
        self.buffer.set_text('')
    
    def on_closeButton_clicked(self, widget, *args):
        dprint ("close clicked")
        self.window.hide()
    
    def on_delete_event(self, widget, *args):
        dprint ("delete signal")
        self.window.hide()
        return True
    
    def on_destroy(self, widget, *args):
        dprint ("destroy signal")
        self.window.destroy()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class AboutDialog(GenericSecondaryWindow):
    """
    Object for the "About Dialog".
    """
    def __init__(self):
        GenericSecondaryWindow.__init__(self, 'aboutDialog', gladefile)
        self.window.set_version(read_version(rootdir))
        self.window.connect("response", lambda d, r: d.destroy())


def generate_file_list():
    import glob
    
    extensions = ['jpg', 'jpeg', 'png', 'tif', 'tiff']
    file_list = []
    for e in extensions:
        for ee in [e.lower(), e.upper(), e.title()]:
            file_list += glob.glob('*.'+ee)
    
    return file_list


def main():
    file_name = sourcedir + 'samples/test-img.tif'
    files = sys.argv[1:]
    if len(files) < 1: files =(file_name,)
    generate_file_list()
    for filename in files:
        try:
            file = open(filename,'r')
            ia = ImageApp(filename)
            file.close()
        except IOError:
            print "\n File '"+filename+"' is not existent or not readable.\n"
            ia = ImageApp()
        ia.start()

if __name__ == "__main__": main()

