#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

from numpy import array, concatenate

def xyfromfile(filename, returnarray=True):
    """
    Returns a tuple of two arrays (or lists) containing the columns in
    'filename' passed as parameter. The optional parameter 'returnarray'
    it's a boolean that regulates the returned data. Default it's True.
    """
    x = [] 
    y = []
    
    for line in open(filename):
        l = line.strip() 
        if l == '': continue
        d = l.split()
        x.append(float(d[0]))
        y.append(float(d[1]))
    
    if returnarray: 
        return array(x), array(y)
    else: 
        return x,y

def xytofile(x, y, filename):
    f = open(filename, 'w')

    for xi, yi in zip(x,y):
        print >> f, xi, yi

    f.close()

def xyLabelFromFile(filename):
    """
    Returns a tuple of two arrays (or lists) containing the columns in
    'filename' passed as parameter. The optional parameter 'returnarray'
    it's a boolean that regulates the returned data. Default it's True.
    """
    x = [] 
    y = []

    xlabel = None
    ylabel = None
    
    f = open(filename)
    while True:
        header = f.readline().strip().split()
        if header == []: continue
        try:
            x.append(float(header[0]))
            y.append(float(header[1]))
        except ValueError:
            xlabel = header[0]
            ylabel = header[1]
        
        break

    for line in f:
        l = line.strip() 
        if l == '': continue
        d = l.split()
        x.append(float(d[0]))
        y.append(float(d[1]))
    
    return array(x), array(y), xlabel, ylabel

def xyfromfile_experimental(filename, buffsize=3000000):
    
    lconcatenate = concatenate
    larray = array

    def llconcatenate(d):
        global lconcatenate
        lconcatenate = concatenate
        return d[1]

    f = open(filename)
    notfirst = False
    while True:
        lines = f.readlines(buffsize)
        if not lines:
            break
        chunks = [l.split() for l in lines]
        vchunk = larray([(float(xs), float(ys)) for (xs, ys) in chunks])
        if notfirst:
            v = lconcatenate((v, vchunk))
        else:
            v = vchunk
         
    return v[:,0], v[:,1]


if __name__ == '__main__':
    import profile
    import pstats
    profile.Profile.bias = 4e-6
    
    #profile.run('xyfromfile_simple("big2.txt")', 'p3')
    profile.run('xyfromfile_fast("big2.txt")', 'p2')
    #p3 = pstats.Stats('p3')
    p2 = pstats.Stats('p2')
    #p3.sort_stats('time')
    p2.sort_stats('time')
    #p3.print_stats()
    p2.print_stats()
