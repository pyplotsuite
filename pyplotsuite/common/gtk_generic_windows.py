#   GktGW -- A set of "template" classes to quick make Glade+pygtk apps.
#   Version: 0.1
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This library is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

"""
Abstract classes for Gtk Windows and dialogs described by Glade files.
"""

import sys

try:
    import pygtk
    pygtk.require("2.0")
except:
    pass

try:
    import gtk
    import gtk.glade
except:
    print '\n You need to install pyGTK or GTK 2.x\n'
    sys.exit(1)

try:
    from matplotlib.figure import Figure
except:
    print '\n You need to install Matplotlib (see README file).\n'
    sys.exit(1)

# Import from matplotlib the FigureCanvas with GTKAgg backend
from matplotlib.backends.backend_gtkagg \
        import FigureCanvasGTKAgg as FigureCanvas

# Import the matplotlib Toolbar2
from matplotlib.backends.backend_gtk \
        import NavigationToolbar2GTK as NavigationToolbar

debug = False

class GenericWindow:
    """
    Abstract class for a window described in a Glade file.
    """
    def __init__(self, windowname, gladefile, autoconnect=True):
        self.windowname = windowname
        self.widgetTree = gtk.glade.XML(gladefile, self.windowname)
        self.window = self.widgetTree.get_widget(self.windowname)
        if autoconnect: self.widgetTree.signal_autoconnect(self)


class GenericMainWindow(GenericWindow):
    """ 
    Abstract class to be enherited by a main window.
    """
    def start(self):
        gtk.main()
    def on_delete_event(self, widget, *args):
        if debug: print 'delete_event:', widget
        return False
    def on_destroy(self, widget, data=None):
        if debug: print 'destroy:', widget
        gtk.main_quit()


class GenericPlotWindow(GenericWindow):
    """
    This class implements a generic plot window with (optional) matplotlib 
    toolbar.
    """
    def __init__(self, windowname, gladefile, autoconnect=True, 
            makeAxis=True, makeToolbar=True):
        GenericWindow.__init__(self, windowname, gladefile, autoconnect)

        # Create the figure, the axes and the canvas
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        if makeAxis:
            self.axis = self.figure.add_subplot(111) 
        
        # Add the canvas to the container
        self.container = self.widgetTree.get_widget('CanvasAlignment')
        self.container.add(self.canvas)
        
        # Set the status bar
        self.statusBar = self.widgetTree.get_widget('statusbar')
        self.context = self.statusBar.get_context_id('Messagges')
       
        # Create the matplotlib toolbar (if requested)
        if makeToolbar:
            self.toolbar = NavigationToolbar(self.canvas, self.window)
            toolbar_container = self.widgetTree.get_widget('ToolbarAlignment')
            toolbar_container.add(self.toolbar)
        
    def writeStatusBar(self, string):
        self.statusBar.push(self.context, string)

    def on_quit_activate(self, widget, *args):
        """
        Menu exit command callback.
        """
        self.on_destroy(widget, *args)


class GenericMainPlotWindow(GenericPlotWindow, GenericMainWindow):
    pass


class GenericSecondaryWindow(GenericWindow):
    """
    Abstract class to be enherited by each "non-main" window.
    """
    def on_delete_event(self, widget, *args):
        if debug: print 'delete_event:', widget
        return False

    def on_destroy(self, widget, data=None):
        # Instead of quitting from gtk, just destroy the window
        if debug: print 'destroy:', widget
        self.window.destroy()


class GenericSecondaryPlotWindow(GenericPlotWindow, GenericSecondaryWindow):
    pass


class ChildWindow(GenericSecondaryWindow):
    """
    Abstract class to be enherited by each window spawed by the main window.
    """
    def __init__(self, windowname, gladefile, callerApp, autoconnect=True):
        GenericSecondaryWindow.__init__(self, windowname, gladefile, 
                autoconnect)
        self.callerApp = callerApp
    

class DialogWindowWithCancel(ChildWindow):
    """
    Abstract class to be enherited by each dialog window with a cancel button.
    """
    def on_cancelButton_clicked(self, widget, *args):
        if debug: print 'Cancel clicked', widget
        self.window.destroy()


class GenericOpenFileDialog(DialogWindowWithCancel):
    """
    This class implements the generic code for an "Open File" dialog.
    """
    def __init__(self, windowname, gladefile, callerApp):
        DialogWindowWithCancel.__init__(self, windowname, gladefile, callerApp)

    def openSelectedFile(self):
        # Override this method to load the file in your app
        # self.filename contains the filename selected by the user
        pass

    def on_openButton_clicked(self, widget, *args):
        if debug: print "Open button clicked", self.window.get_filename()
        self.filename = self.window.get_filename()
        if self.filename != None:
            self.openSelectedFile()

    def on_openfileDialog_file_activated(self, widget, *args):
        if debug: print "Open File Dialog: file_activated", \
                self.window.get_filename()

    def on_openfileDialog_response(self, widget, *args):
        print "\nOpen File Dialog: response event"

class GenericSaveFileDialog(DialogWindowWithCancel):
    """
    This class implements a generic "Save File" dialog.
    """
    def __init__(self, windowname, gladefile, callerApp):
        DialogWindowWithCancel.__init__(self, windowname, gladefile, callerApp)

    def saveToSelectedFile(self):
        # Override this method to load the file in your app
        # self.filename contains the filename selected by the user
        pass

    def on_saveButton_clicked(self, widget, *args):
        if debug: print "save button clicked", self.window.get_filename()
        self.filename = self.window.get_filename()
        if self.filename != None:
            self.saveToSelectedFile()

    def on_openfileDialog_file_activated(self, widget, *args):
        if debug: 
            print "Save File Dialog: file_activated", self.window.get_filename()

    def on_openfileDialog_response(self, widget, *args):
        if debug: print "\nSave File Dialog: response event"

    def on_cancelButton_clicked(self, widget, *args):
        # Don't destroy the dialog so we remeber the folder next time
        self.window.hide()
        return True

class MyNavigationToolbar(NavigationToolbar):
    """
    This class is derived from the matplotlib Toolbar2 with one toggle button
    added at the end. 
    """
    def __init__(self, canvas, window, ButtonCallBack, icon_fname,
            label=None, tooltip=None):
        NavigationToolbar.__init__(self, canvas, window)
        icon = gtk.Image()
        icon.set_from_file( icon_fname )
        button = gtk.ToggleToolButton(None)
        button.set_label(label)
        button.set_icon_widget(icon)
        button.connect('toggled', ButtonCallBack)
        button.show_all()
        # I don't know why the tooltips don't work here
        self.set_tooltips(True)
        tips = gtk.Tooltips()
        tips.set_tip(button, tooltip)
        tips.enable()
        self.insert(button, 8)

