#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

from numpy import array, arange, sin, cos, pi

def circle(xcenter, ycenter, radious, Npoints=40):

    t = arange(Npoints+1)/float(Npoints)

    x = -radious * cos(2*pi*t) + xcenter
    y = -radious * sin(2*pi*t) + ycenter

    return x, y

def square(xcenter, ycenter, side_length):
    
    dl = side_length / 2.

    x = array([xcenter-dl, xcenter+dl, xcenter+dl, xcenter-dl, xcenter-dl])
    y = array([ycenter-dl, ycenter-dl, ycenter+dl, ycenter+dl, ycenter-dl])
    return x, y


if __name__ == '__main':
    from pylab import *
    from scipy import lena
    
    x, y = circle(2,-4, 3, 50)
    x1, y1 = square(1,-2,1.5)
    plot(x,y, '--')
    plot(x1,y1, '-ko')
    show()



