#!/bin/env python
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

from PIL import Image
from numpy import array, ones, zeros, sqrt, ceil, floor
from pyplotsuite.common.img import image2array

def arrayFromTiff(filename):
    """
    Returns and array loading the data from the TIFF file 'filename'.
    """
    return image2array(Image.open(filename))

def arrayfromfile(file, xmin=None, xmax=None, debug=False):
    """
    Load (and returns) an array fron an ASCII text file. 'file' is the opened 
    file object.
    
    Optional 'xmin' and 'xmax' permit to load only a slice of colums (for
    example excluding the fistr and the last).
    """
    
    if xmin == None: start = 0
    else: start = xmin

    if xmax == None: end = -1
    else: end = xmax

    list_of_rows = []
    i = 1
    for line in file:
        if line.strip() == '': continue
        data = line.split()
        if end == -1: row_data = [float(s) for s in data[start:]]
        else: row_data = [float(s) for s in data[start:end+1]]
        if debug: print i, 'len:', len(row_data)
        list_of_rows.append(row_data)
        i += 1
        
    m = array(list_of_rows)
    return m

def arrayFromZemax(zemaxfile, xmin=None, xmax=None, zemaxversion=2003):
    """
    Returns an array loading the data from a Zemax simulation file. Zemax
    simulation file are ASCII file with a particular header and syntax. See the
    code for more information.

    Eventual 'xmin' and 'xmax' parameters will be passed to arrayfromfile().
    """
    
    def skip_n_lines(n):
        for i in xrange(n): zemaxfile.next()
    
    # Go to the beginning of the file for robustness
    zemaxfile.seek(0)
    
    if zemaxversion == 2003:
        lines_to_skip = 19  ## 2003 Version
    else:
        lines_to_skip = 23  ## 2005 Version

    
    for line in zemaxfile:
        if line.startswith('Detector Viewer listing'):
            skip_n_lines(lines_to_skip)
            zm = arrayfromfile(zemaxfile)[:,1:]
        elif line.startswith('Illumination Surface Listing'):
            skip_n_lines(12)
            zm = arrayfromfile(zemaxfile, xmin=xmin, xmax=xmax, debug=True)
        else:
            print 'arrayFromZemax(): Invalid file format.'
            zm = None
        return zm


