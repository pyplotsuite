# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

def read_version(rootdir):
    """ Extract version from the VERSION file. """
    file = open(rootdir+'VERSION')
    ver = file.readline().strip()
    file.close()
    return ver

