#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

import sys
from PIL import Image
from numpy import fromstring, resize, array, around, arange, zeros

class WrongImageFormat(Exception):
    """ Exception for image format not known. """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def image2array(image):
    """ Converts an image object (PIL) to an array (NumPy). """
    if image.mode == "RGB":
        dimensions = (image.size[1], image.size[0], 3)
        type = 'uint8'
    elif image.mode == "RGBA":
        dimensions = (image.size[1], image.size[0], 4)
        type = 'uint8'
    elif image.mode == "L":
        dimensions = (image.size[1], image.size[0])
        type = 'uint8'
    elif image.mode == "I;16":
        dimensions = (image.size[1], image.size[0])
        type = 'uint16'
    else:
        raise WrongImageFormat, "Image type "+image.mode+" unknown."

    image_string = image.tostring()
    image_array = fromstring(image_string, type)
    return resize(image_array, dimensions)

def image2array2(image):
    """Converts an image object (PIL) into an array (NumPy).
    Seems slightly slower than image2array()."""
    if image.mode == "RGB":
        dimensions = (image.size[1], image.size[0], 3)
        type = 'UInt8'
    elif image.mode == "RGBA":
        dimensions = (image.size[1], image.size[0], 4)
        type = 'UInt8'
    elif image.mode == "L":
        dimensions = (image.size[1], image.size[0])
        type = 'UInt8'
    elif image.mode == "I;16":
        dimensions = (image.size[1], image.size[0])
        type = 'UInt16'
    else:
        raise WrongImageFormat, "Image type "+image.mode+" unknown."

    image_array = array(image.getdata(), dtype='UInt16')
    return resize(image_array, dimensions)

def histogram3(image, nbit):
    """
    It is used instead of image.histogram() because the latter does not work
    with high dynamic images (for example 14 bit).

    Takes as parameters a PIL image and the number of bit and returns an array
    2**nbit length containing the image histogram.

    This function has been written by Fredrik Lundh, in the article:
        
        http://effbot.org/zone/image-histogram-optimization.htm
    
    This code snipped is public domain as stated at:

        http://effbot.org/zone/copyright.htm
    """
    
    # wait, use a list, but get rid of that getpixel call
    result = [0] * 2**nbit
    for v in image.getdata():
        result[v] = result[v] + 1
    return result

def histogramfloat(farray, nbit=10, rescale=True):
    """
    Calculates (quite) efficiently the histogram of a float array. 
    
    Returns two arrays of the same length: 
     - an equally spaced array of (quantized) values 
     - the occurrence of each value

    The optional 'nbit' parameter set the quantization number of bit.
    The optional 'rescale' flag can inhibit data rescaling. This is useful if
    your data comes from a source with a known number of bit (for example an
    image converted to array). In this case you must set the correct 'nbit' 
    and assure that the 'farray' data is contained in the range 0..(2^nbit)-1 
    (extreme included).
    """
    
    if rescale: 
        m = farray.max()
    else:
        m = (2.**nbit) - 1
    
    rarray = around(farray*((2.**nbit)-1)/m).astype(int)
    values = arange(2**nbit)*m/(2.**nbit-1)
    
    hist_data = zeros(2**nbit)
    for v in rarray.ravel():
        hist_data[v] += 1
    return values, hist_data

def speed_test():
    files = sys.argv[1:]
    if len(files) < 1:
        files = ['test-img.tif']
    
    for file_name in files:
        try: 
            image = Image.open(file_name)
        except IOError:
            print "\n File '"+file_name+"' cannot be opened.\n"
            sys.exit()    
            
        profile.run('ia = image2array(image)', 'ia')
        profile.run('ia2 = image2array2(image)', 'ia2')

if __name__ == "__main__":
    from pylab import *
    from scipy import lena
    import profile

    a = lena()
    v, h = histogramfloat(a, nbit=8, rescale=False)
    step = 1
    print v[0], v[-1]
    print v[20:30]
    print h[20:30]
    
    plot(v, h, 'ro')
    bar(v,h, width=step, bottom=0.01, edgecolor='blue')
    grid(True)
    show()



