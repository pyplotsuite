#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

"""
Provides some UnivariateSpline child classes for alternative interpolation
methods.
"""

from numpy import log, exp, e
import scipy.interpolate as SI

class eSpline(SI.UnivariateSpline):
    """
    Spline in Y log scale (with fized e base). Simple prototype class.
    """
    
    def __init__(self, x, y, w=None, bbox=[None, None], k=3, s=None):
        """
        For a description of the parameters see UnivariateSpline class.
        """

        y_ = log(y)
        SI.UnivariateSpline.__init__(self, x=x, y=y_, s=s, k=k, bbox=bbox, w=w)
    
    def __call__(self, x, nu=None):
        res = SI.UnivariateSpline.__call__(self, x, nu)
        return exp(res)


class aSpline(SI.UnivariateSpline):
    """
    Base class for splines calculated in a generic coordinate system.
    """
    def __init__(self, x, y, fun, inv, both=False, 
            w=None, bbox=[None, None], k=3, s=None):
        """
        Besised the starndard UnivariateSpline parameters we have:
        @fun: the function to be applied to the input data 'y' and 'x'
        @inv: the inverse function to be applied to the returned data
        @both: if the transformation should be applied to both axes or only Y
        """
        self.__fun = fun
        self.__inv = inv
        self.both = both
        y_ = fun(y)
        if both:
            x_ = fun(x)
        else:
            x_ = x
        SI.UnivariateSpline.__init__(self, x=x_, y=y_, s=s, k=k, 
                bbox=bbox, w=w)
    
    def __call__(self, x, nu=None):
        """
        Same parameters as UnivariateSpline.
        """
        if self.both:
            x_ = self.__fun(x)
        else:
            x_ = x
        res = SI.UnivariateSpline.__call__(self, x_, nu)
        return self.__inv(res)


class eeSpline(aSpline):
    """
    Exponential spline: a spline calculated in log scale.
    """
    
    def __init__(self, x, y, base=e, w=None, bbox=[None, None], k=3, s=None):
        """
        Besides usual UnivariateSpline parameters a 'base' for the exponetial
        transformation can be specified.
        """
        self.base = base
        
        # Workaround for bad interpolation with s=0
        if s == 0: s = len(x)*1e-6
        
        aSpline.__init__(self, x=x, y=y, both=False,
                fun = lambda x: log(x)/log(self.base),
                inv = lambda x: self.base**x,
                s=s, k=k, bbox=bbox, w=w)


