#
#   Copyright (C) 2006-2007 Antonino Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

from numpy import logical_and, logical_or, logical_not, arange, sqrt 

def peak_integral1(a, t, xc, yc, r):
    
    # Creating distance function using numpy broadcasting rules
    x = arange(a.shape[1])
    y = arange(a.shape[0]).reshape(a.shape[0],1)
    distance = sqrt((x-xc)**2 + (y-yc)**2)

    # Creating domain and residual_domain for the integral
    domain_inside_circle = (distance < r)
    if t > 0:
        domain_above_threshold = (a > t)
        domain_below_threshold_abs = logical_and((a < t), (a > -t))
    else:
        # for t <= 0 select all the array
        domain_above_threshold = True
        domain_below_threshold_abs = True
    domain_index = logical_and(domain_above_threshold,domain_inside_circle)
    domain_size = domain_index.sum()
    domain = a[domain_index]
    residual_domain = \
            a[logical_and(logical_not(domain_inside_circle),
                    domain_below_threshold_abs)]

    # Calculating integrals
    raw_integral = float(domain.sum())
    raw_residual = float(residual_domain.sum())

    # Offset compensation
    offset = raw_residual / residual_domain.size
    offset_integral = raw_integral - offset*domain.size

    return raw_integral, domain_size, raw_residual, offset_integral, offset

def peak_integral2t(a, t, rt, xc, yc, r, use_circle=True, use_threshold=True):
    
    # Creating "distance" function using numpy broadcasting rules
    x = arange(a.shape[1])
    y = arange(a.shape[0]).reshape(a.shape[0],1)
    distance = sqrt((x-xc)**2 + (y-yc)**2)

    # Creating domain and residual_domain for the integral
    if use_circle:
        domain_inside_circle = (distance < r)
    else:
        domain_inside_circle = True
    
    if t > 0 and use_threshold:
        domain_above_threshold = (a > t)
    else:
        # for t <= 0 or use_threshold==False select all the array
        domain_above_threshold = True
    if rt > 0 and use_threshold:
        domain_below_threshold_abs = logical_and((a < rt), (a > -rt))
    else:
        # for rt <= 0  or use_threshold==False select all the array
        domain_below_threshold_abs = True

    domain_index = logical_and(domain_above_threshold,domain_inside_circle)
    #domain_size = domain_index.sum()
    domain = a[domain_index]
    residual_domain = \
            a[logical_and(logical_not(domain_inside_circle),
                    domain_below_threshold_abs)]

    # Calculating integrals
    raw_integral = float(domain.sum())
    raw_residual = float(residual_domain.sum())

    # Offset compensation
    offset = raw_residual / residual_domain.size
    offset_integral = raw_integral - offset*domain.size

    return raw_integral, domain.size, raw_residual, offset_integral, offset

if __name__ == "__main__":
    from PIL import Image
    from pylab import *
    import profile
    import time
    import scipy.ndimage.measurements as M
    from numpy import log

    print 'Begin'
    fname='samples/test-img.tif'
    a = image2array(Image.open(fname)) + 0.1
    i = Image.open(fname)
    m = a.max()
    m2 = int(round(m+1))
    t1 = time.time()
    raw_integral, domain_size, raw_residual, offset_integral, offset = \
            peak_integral2(a, 335, 264, 227, 100)
    t2 = time.time()
    print 'tempo: ', t2-t1
    print a.dtype

