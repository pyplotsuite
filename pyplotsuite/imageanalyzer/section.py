#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.


from numpy import arange, sign, array, sqrt

def rounda(array):
    """Round the array passed to the nearest integer."""
    return floor(array + 0.5).astype('int32')

class ArraySection:
    """
    Class representing an array section. See the __init__ documetation.
    """
    def __init__(self, arr, x1, x2, y1, y2, xPixDim, yPixDim, debug=False):
        """
        Parameters
        arr:    arrray to section
        x1, x2, 
        y1, y2: coordinates of the twop point indentifing the segment
                along which to calculate the array values (the section).
                This params are in array-index coordinates.
        xPixDim,
        yPixDim: X and Y dimention of one pixel (element) of the array
        debug:  optionally enable debug prints
        
        Class Attributes:
        self.y: the array values along the section
        self.x: the X axis for self.y calculated considering the effective
                section length (considering pixel dimetion and the slope).
        self.dim: real section length calculated based on pixel dimention and
                  section slope.
        self.color: Default to None, you can fill this to associate a color
                    to the section
        """
        # Calculate section values
        self.y = sectionarray(arr, x1, x2, y1, y2, debug=debug)
        self.dim = sqrt( ((x2-x1)*xPixDim)**2 + ((y2-y1)*yPixDim)**2 )
        self.x = arange(len(self.y))/float(len(self.y)-1) * self.dim
        
        # The caller can use this
        self.color = None
        
        ## Save some data about the segment
        #self.x1, self.x2, self.y1, self.y2, self.xPixDim, self.yPixDim =\
        #        x1, x2, y1, y2, xPixDim, yPixDim
 
        # Provided for further extensions (currently unused)
        #self.plotkwargs = dict()


def sectionarray(arr, x1, x2, y1, y2, debug=False):
    """
    Returns the pixel-precise section of the array between the two points (x1,
    y1) and (x2, y2) (extremezes included).

    """
    dx = x2 - x1
    dy = y2 - y1
    n = max(abs(dx),abs(dy))
    if debug: print 'dx:', dx, 'dy:', dy, 'n:', n

    if dx == 0 and dy == 0:
        # Single point
        if debug: print 'point segment'
        section = arr[x1, y1]
    elif dx == 0:
        # Vertical segment
        if debug: print 'vertical segment'
        y = arange(y1, y2+sign(dy), sign(dy))
        section = arr[y, x1]
    elif dy == 0:
        # Horizzontal segment
        if debug: print 'orizzontale segment'
        x = arange(x1, x2+sign(dx), sign(dx))
        section = arr[y1, x]
    else:
        # Skew segment
        if debug: print 'skew segment'
        m = float(dy) / float(dx)
        c = y1 - m*x1
        step = float(dx) / float(n)
        if debug: print 'm:',m,'c:',c,'step:',step
        X = arange(x1, x2+step, step)
        Y = m * X + c

        arr = array(arr)
        section = arr[Y.round().astype('uint'), X.round().astype('uint')]

    return section

if __name__ == "__main__":
    from scipy import lena
    from pylab import plot, imshow, title, grid, gcf, gca, show, draw
    l = lena()
    imshow(l)
    title('Click on two points the close the window')
    
    points = []
    def button_press_callback(event):
        points.append([event.xdata, event.ydata])
        gca().plot((event.xdata,), (event.ydata,), 'k', marker='.', ms=10)
        draw()

    gcf().canvas.mpl_connect('button_press_event', button_press_callback)
    show()
    
    c = ArraySection(l, 
            points[-2][0], points[-1][0],
            points[-2][1], points[-1][1], 1, 1)

    plot(c.x, c.y, '-b.', linewidth=1, ms=10)
    title('Image section')
    grid(True)
    show()





