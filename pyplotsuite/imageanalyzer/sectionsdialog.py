#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

from pyplotsuite.common.gtk_generic_windows import GenericSecondaryPlotWindow

class SectionApp(GenericSecondaryPlotWindow):
    """
    This class implements the window to show the image's sections.
    """
    def __init__(self, gladefile, section, callerApp, showpoints=False,
            debug=False):
        GenericSecondaryPlotWindow.__init__(self, 'SectionWindow', 
                gladefile)
        
        self.callerApp = callerApp
        self.debug = False

        self.sections = []
        self.showpoints = False
        self.addsection(section, showpoints=self.showpoints)

        self.window.show_all()

    ##
    # Methods exported to manipulate the istance object
    #
    def addsection(self, section, showpoints=None):
        if showpoints != None: self.showpoints = showpoints

        # Create the diagram
        self.plot_section(section, self.showpoints)
        self.axis.grid(True)
        self.canvas.draw()
        # Save the current section
        self.sections.append(section)

    def plot_section(self, section, showpoints=False):
        options = {}
        if showpoints:
            options = {'marker': '.', 'ms': 10, 'mfc': section.color}
        print 'plot_section: color:', section.color
        self.axis.plot(section.x, section.y, color=section.color, lw=1, 
                **options)

    def plot_all(self, showpoints=None):
        if showpoints != None: self.showpoints = showpoints
        self.axis.clear()
        for sect in self.sections:
            self.plot_section(sect, self.showpoints)
        self.axis.grid(True)
        self.canvas.draw()

    def destroy(self):
        """
        Method to be used by callers Apps to destroy the object.
        """
        self.on_destroy(None)

    ##
    # The following are GUI callbacks
    #
    def on_gridCheckButton_toggled(self, widget, data=None):
        self.axis.grid()
        self.canvas.draw()

    def on_pointCheckButton_toggled(self, widget, data=None):
        self.showpoints = not self.showpoints
        if self.debug: print 'Show points:', self.showpoints
        self.plot_all(self.showpoints)

    def on_closeButton_clicked(self, widget, *args):
        self.window.hide()

    def on_destroy(self, widget, data=None):
        if self.debug: print 'idestroy:', widget
        self.callerApp.sectionApp = None
        self.window.destroy()


