#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.


class CircularList:
    """
    CircularList is class that contains a fixed-size list. The caracteristic of
    this class is that the .next() method never raises a StopIteration: after 
    the last element it returns the first again, and so on. The size of the
    list is required by the constructor.
    
    Methods:
    .append()       similar to the append method for the list. Can be used to
                    populate the list.
    .next()         it retuns the items of the list using a FIFO policy. After
                    the last items it returns agin the first item, and so on.
    .resetIndex()   it resets the internal index used to provide the .next
                    item. After this method is called the item returned by
                    .next is always the first item.
    .get()          get an element by index.
    .fromlist()     initialize the CircularList from a regular list.
    """
    def __init__(self, size):
        self.data = [[] for i in xrange(size)]
        self.__size = size
        self.__nextindex = 0
        self.__putindex = 0
    def next(self):
        if self.__nextindex == self.__size : self.__nextindex = 0
        self.__nextindex += 1
        return self.data[self.__nextindex-1]
    def append(self, item):
        if self.__putindex == self.__size:
            self.__putindex = 0
        self.data[self.__putindex] = item
        self.__putindex += 1
    def resetIndex(self):
        self.__putindex = 0
        self.__nextindex = 0
    def fromlist(self, list):
        """
        Initialize the CircularList fron the data contained in 'list'.
        len(list) must be equal to the CircularList len.
        """
        if len(list) != self.__size:
            raise(IndexError, 
                "The list must have the same length of the CircularList")
            return
        self.data = list
        self.resetIndex()
    def get(self, index):
        """Returns the item with the given "index", doing the wrapping"""
        return self.data[index - (index//self.__size)*self.__size]
    def __str__(self):
        return str(self.data)
 

if __name__ == "__main__":
    # Some tests
    n = 10
    a = CircularList(n)
    print a
    a.fromlist(range(n))
    print a
    print "Firsts", a.get(0), a.get(10), a.get(20)
    print "Second", a.get(1), a.get(11), a.get(21)
    print "Third", a.get(2), a.get(12), a.get(22)
    
    for i in xrange(17):
        print a.next()
    
    for i in xrange(32,46):
        a.append(i)
    print a
    
    a.append('current')
    a.resetIndex()
    a.append('first')
    a.append('second')
    print a.next()
    print a.next()
    print a

