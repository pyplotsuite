#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

"""
Functions for 2D arrays resampling.
"""

import scipy.signal as SS
import scipy.fftpack as SF
#import SGfilter
from numpy import array, empty

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def resample2d(a, y_samples=None, x_samples=None, window=None):
    """
    Fast 2D array resampler (sub- and over- sampling) with Fourier filtering
    and optional windowing. See scipy.signal.resample for further information 
    about the various windows.

    For a slightly faster version see resample2d_fast.
    """
    ly, lx = a.shape
    if x_samples == None: x_samples = lx/2
    if y_samples == None: y_samples = ly/2

    res = SS.resample(a, x_samples, axis=0, window=window) 
    res = SS.resample(res, y_samples, axis=1, window=window)
    return res

def resample2d_fast(a, y_samples=None, x_samples=None):
    """
    Fast 2D array resampler (sub- and over- sampling) with Fourier ideal 
    filtering. This function is around 25% faster that reample2d but doesn't
    have the optional windowing feature that sometime can give better quality.

    The speedup is due to direct use of scipy fft routines instead of resample.
    """
    ly, lx = a.shape
    if x_samples == None: x_samples = lx/2
    if y_samples == None: y_samples = ly/2

    # X Axis resampling (note x_samples specified only in irfft)
    res = SF.irfft(SF.rfft(a, axis=1), x_samples, axis=1)\
            *x_samples/float(lx)
    
    # Y Axis resampling (note y_samples specified only in irfft)
    res = SF.irfft(SF.rfft(res, axis=0), y_samples, axis=0)\
            *y_samples/float(ly)
    
    return res

#def resample2d_SG(a, dec=2):
#    """
#    A decimator that uses the Savitzky-Golay filter.
#    
#    This decimator is very fast for large data (>> 1000) and has exellect 
#    result for strong decimation values (10 or more).
#
#    @param a: the 2D array to decimate.
#    @param dec: how much decimate (integer >= 1).
#    
#    @Return: The filtered/decimated 2D data.
#
#    @Note: For dec = 1 its only a filter.
#    """
#    ly, lx = a.shape
#    res = empty((ly, ceil(lx/float(dec))))
#    res2 = empty((ceil(ly/float(dec)), ceil(lx/float(dec))))
#    #print res2.shape
#    #print SGfilter.Smooth(a[0,:], decimate=dec).shape
#    for i, row in enumerate(a):
#        res[i,:] = SGfilter.Smooth(row, decimate=dec)
#    for i, col in enumerate(transpose(res)):
#        res2[:,i] = SGfilter.Smooth(col, decimate=dec)
#
#    return res2

def digital_binning(a, xb, yb=None):
    """
    Implement a digital binning via software similar to the binning feature
    present in some CCD.

    @param a: the 2D numpy array to be binned
    @param xb: binning factor in the x direction
    @param yb: binning factor in the y direction, if not specified is assumes
        the same as xb

    @Return: A sub-sampled array with the binned values.
    """
    if yb is None: yb = xb
    
    ba = empty((a.shape[0]/yb, a.shape[1]/xb))

    for iby, iy in enumerate(xrange(0, a.shape[0]-yb+1, yb)):
        for ibx, ix in enumerate(xrange(0, a.shape[1]-xb+1, xb)):
            ba[iby,ibx] = a[iy:iy+yb,ix:ix+xb].sum()

    return ba


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def test_resample():
    d = 3

    t1 = time()
    f = resample2d(lena(), 512/d, 512/d, window=None) 
    t2 = time()
    print t2-t1
    
    t1 = time()
    fo = resample2d_fast(lena(), 512/d, 512/d) 
    t2 = time()
    print t2-t1
    
    t1 = time()
    fob = digital_binning(lena(), xb=d) 
    t2 = time()
    print t2-t1

    figure()
    imshow(lena(), vmin=0, vmax=255, interpolation='nearest')
    colorbar()    
    title('Original Image')

    figure()
    imshow(f, vmin=0, vmax=255, interpolation='nearest')
    colorbar()    
    title('Resampled Immage with FFT (window)')

    figure()
    imshow(fo, vmin=0, vmax=255, interpolation='nearest')
    colorbar()    
    title('Resampled Immage with FFT (fast)')

    figure()
    imshow(fob/d**2, vmin=0, vmax=255, interpolation='nearest')
    colorbar()    
    title('Reduced image with binning')

def test_binning():
    a = lena()

    print " + Calculating binning ... ",
    t1 = time()
    b = digital_binning(a, 3,3)
    t2 = time()
    print "OK (%1.2f seconds)" % (t2-t1)
    
    print b.shape
    imshow(b, interpolation='nearest', aspect='equal')
    grid()
    show()


if __name__ == '__main__':
    import sys
    from time import time
    from pylab import *
    from numpy import *
    from scipy import lena
    #from array_utils import digital_binning

    #test_resample()
    test_binning()
    show()

