#
#   Copyright (C) 2006-2007 Antonino Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

from numpy import array, arange, min
from matplotlib.widgets import SpanSelector
from pyplotsuite.common.gtk_generic_windows import GenericSecondaryPlotWindow
try:
    import scipy.ndimage.measurements as M
except ImportError:
    try:
        import numarray.nd_image.measurements as M
    except ImportError:
        has_measurements = False

class HistogramApp(GenericSecondaryPlotWindow):
    """
    This class implements the histogram window.
    """
    def __init__(self, gladefile, callerApp, bits=14, debug=True):
        GenericSecondaryPlotWindow.__init__(self, 'HistogramWindow', gladefile, 
                autoconnect=False)
        self.callerApp = callerApp
        self.min, self.max = self.callerApp.min, self.callerApp.max
        self.debug = debug

        # Calculate the histogram
        self.n = 250
        self.m = min((0, self.callerApp.image_array.min()))
        self.Max = self.callerApp.image_array.max()
        self.calculate_hist()
       
        # Plot the histogram
        self.yscale = 'log'
        self.showPoints = True
        self.showLines = True
        self.showBars = False
        self.replot = True
        
        if self.showBars: self.showPoints, self.showLines = False, False

        self.selection = None       # Range selected (axvspan instance)
        self.plot_histogram()

        # Spin Buttons handlers
        self.widthSpinButt = self.widgetTree.get_widget('widthSpinButt')
        self.positionSpinButt = self.widgetTree.get_widget('positionSpinButt')
        self.binsSpinButt = self.widgetTree.get_widget('binsSpinButt')

        # Check Buttons handlers
        self.pointsCheckButt = self.widgetTree.get_widget('pointsCheckButton')
        self.linesCheckButt = self.widgetTree.get_widget('linesCheckButton')
        self.barsCheckButt = self.widgetTree.get_widget('barsCheckButton')

        # Create the span selector, link to the CB, but make it not visible
        self.span = self.create_span()
        self.span.visible = False

        # Set the SpinButtons to the right values
        self.called_by_onspanselect = False
        self.set_selection(self.callerApp.min, self.callerApp.max)
        self.binsSpinButt.set_value(self.n)

        # Connect the CB now to avoid CB calls during the setup of SpinButtons
        self.widgetTree.signal_autoconnect(self)
        self.window.show_all()

    def dprint(self, *args):
        if self.debug: print args

    def calculate_hist(self):
        self.x = arange(self.n)*self.Max/float(self.n-1)
        self.y = M.histogram(self.callerApp.image_array, 
                self.m, self.Max, self.n) 
        self.dprint ('x', len(self.x), 'y', len(self.y))
    
    def create_span(self):
        self.dprint ("create span")
        span = SpanSelector(self.axis, self.onspanselect,
                'horizontal', useblit=True, minspan=50)
        return span

    def plot_histogram(self, showPoints=None, showLines=None):
        if not self.replot: return
        
        if showPoints != None: self.showPoints = showPoints
        if showLines != None: self.showLines = showLines

        options = {'ls': '', 'marker': ''}
        if self.showPoints and self.showLines:
            options = {'ls': '-', 'marker': '.', 'ms': 10}
        elif not self.showPoints and self.showLines:
            options = {'ls': '-', 'marker': ''}
        elif self.showPoints and not self.showLines:
            options = {'ls': '', 'marker': '.', 'ms': 10}

        # We clear the axis so a change in Y scale could take effect
        # but in this way span.selection becomes an object without axis
        # so we have to reset it too
        self.axis.clear()
        self.selection = None
        # We plot in 'linear' Y scale to avoid problems plotting log scale
        self.axis.set_yscale('linear')
        
        if self.showBars:
            self.axis.bar(self.x, self.y, width=1.0, 
                    bottom=0.01, edgecolor='black')
        else:
            self.line, = self.axis.plot(self.x, self.y, linewidth=1, **options)
       
        self.axis.set_xlabel('Values')
        self.axis.set_ylabel('Number of points')
        self.axis.grid(True)
        # Now the actual Y scale can be safely set
        self.axis.set_yscale(self.yscale)           # 'log' or 'linear'
        if self.yscale == 'log':
            self.axis.set_ylim(ymin=0.2)
        self.plot_selection(self.min, self.max, drawcanvas=False)
        self.canvas.draw()

    def plot_selection(self, min, max, drawcanvas=True):
        """ Plot the rectangle representing the selection. """
        self.dprint ('plot_selection')
        if self.selection == None:
            self.selection = self.axis.axvspan(min, max,
                    facecolor='green', alpha=0.5)
        else:
            self.selection.xy = [(min, 0), (min, 1), (max, 1), (max, 0)]
        if drawcanvas:
            self.canvas.draw()

    def set_selection(self, min, max):
        self.dprint ('set_selection')
        self.positionSpinButt.set_value(min)
        self.widthSpinButt.set_value(max-min)

    def statusminmax(self, min, max):
        self.dprint ('statusminmax')
        self.statusBar.push(self.context,
                'Range: min = '+ str(min) +', max = '+ str(max))

    def onspanselect(self, xmin, xmax):
        self.dprint ('onspanselect')
        min, max = int(xmin), int(xmax)

        self.called_by_onspanselect = True
        self.set_selection(min, max)
        self.called_by_onspanselect = False

        self.plot_selection(min, max)

        self.callerApp.plot_image(min, max)
        self.statusminmax(min, max)
        self.min, self.max = min, max

    ##
    # The following are GUI callback methods
    #
    def on_yscale_toggled(self, widget, *args):
        if self.yscale == 'linear': self.yscale = 'log'
        else: self.yscale = 'linear'
        self.dprint ("YScale:", self.yscale)
        self.axis.set_yscale(self.yscale)
        if self.yscale == 'log':
            self.axis.set_ylim(ymin=0.2)
        
        #self.axis.autoscale_view(scaley=True)
        self.canvas.draw()

    def on_pointsCheckButton_toggled(self, widget, *args):
        self.showPoints = not self.showPoints
        if self.showPoints:
            self.replot = False
            self.barsCheckButt.set_active(False)
            self.replot = True
        self.plot_histogram(showPoints=self.showPoints)

    def on_linesCheckButton_toggled(self, widget, *args):
        self.showLines = not self.showLines
        if self.showLines:
            self.replot = False
            self.barsCheckButt.set_active(False)
            self.replot = True
        self.plot_histogram(showLines=self.showLines)

    def on_barsCheckButton_toggled(self, widget, *args):
        self.showBars = not self.showBars
        if self.showBars:
            self.replot = False
            self.linesCheckButt.set_active(False)
            self.pointsCheckButt.set_active(False)
            self.replot = True
        self.plot_histogram(showLines=self.showLines)

    def on_binsSpinButt_value_changed(self, widget, *args):
        self.n = int(self.binsSpinButt.get_value())
        self.calculate_hist()
        self.line.set_data(self.x, self.y)
        self.canvas.draw()

    def on_SpinButtons_value_changed(self, widget, *args):
        if self.called_by_onspanselect: return
        min = self.positionSpinButt.get_value()
        self.plot_selection(min, min+self.widthSpinButt.get_value())
        self.statusBar.push(self.context,
                 'Press "Apply" to set the changes on the image.')
    
    def on_applyRangeButton_clicked(self, widget, *args):
        self.dprint ('on_applyRangeButton_clicked')
        min = int(self.positionSpinButt.get_value())
        max = min + int(self.widthSpinButt.get_value())
        self.statusminmax(min, max)
        if (min, max) == (self.min, self.max): return
        self.min, self.max = min, max
        self.callerApp.plot_image(min, max)

    def on_closeButton_clicked(self, widget, *args):
        self.window.hide()

    def on_rangeselectTButton_toggled(self, widget, *args):
        self.span.visible = not self.span.visible
        if self.span.visible:
            self.dprint ('Span selector activated')
            self.statusBar.push(self.context,
                    'Now drag the mouse over the plot to select a range.')
        else:
            self.dprint ('Span selector deactivated')
            self.statusBar.push(self.context,
                    'Range selection tool deactivated.')

    def on_destroy(self, widget, data=None):
        self.dprint ('idestroy:', widget)
        self.callerApp.histogramApp = None
        self.window.destroy()

