#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
#   Copyright (C) 2006-2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This file is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   PyPlotSuite is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

import cPickle
import numpy as N
from PIL import Image

from pyplotsuite.common.arrayfromfile import arrayfromfile, arrayFromZemax
from pyplotsuite.common.img import image2array

FLOAT_ARRAY = 'floatarray'
DARK_FNAME_SUFFIX = '-buio'

class UnknownFileType(Exception):
    """ Exception for unknown file types. """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class WrongFileType(Exception):
    """ Exception for (known but) wrong file types. """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def load_file(fname, dark_suffix=DARK_FNAME_SUFFIX):
        """
        Low-level function used by load_image to handle file open, format
        autodetection and loading of basic data.
        """
        
        # Try to open the file, otherwise return immediately
        try:
            file = open(fname)
        except IOError:
            raise   # raise returns automatically
        
        # Try to open the file as a pickle array
        try:
            print file
            image_array = cPickle.load(file)
        except:
            pass
        else:
            # IS a pickle file, check if it's (or can be converted to) an array
            try:
                image_array = N.array(image_array)
            except ValueError:
                raise

            # IS an array, check if it has 2D shape
            if len(image_array.shape) != 2:
                raise WrongFileType('File does not contains a 2D array')

            # Ok, we can safely return the file
            return image_array, FLOAT_ARRAY, None

        if fname.lower().endswith('txt'):
        ## Note: Zemax and ASCII file can have only the txt extension
            image_array = arrayFromZemax(file, zemaxversion=2003)
            if image_array == None:
                image_array = arrayFromZemax(file, zemaxversion=2005)
            
            if image_array == None:
                try:
                    image_array = arrayfromfile(file)
                except:
                    raise UnknownFileType('ASCII file format not supported.')
            
            return image_array, FLOAT_ARRAY, None

        try:
            image = Image.open(fname)
        except IOError:
            raise
        else:
            imagemode = image.mode
            dark_fname = fname[:-4] + dark_suffix + fname[-4:]
            try:
                # Try to load a dark image
                dark_image = Image.open(dark_fname)
            except IOError:
                # If fails to open the dark file, use only the image
                image_array = image2array(image)
            else:
                # Otherwise subtract the dark from the image
                image_array = (N.array(image2array(image)).astype('float') - \
                            N.array(image2array(dark_image)).astype('float'))
            
            return image_array, imagemode, image

        
        print '\n  Error: internal error in load_file():'
        print '  This code should never be executed.\n'
        raise "UnknownError"

