#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   PlotFile2 -- A tool to make quick plots from data series.
#   Version: 0.1-alpha (see VERSION file)
#   This program is part of PyPlotSuite <http://pyplotsuite.sourceforge.net>.
#
#   Copyright (C) 2007 Antonio Ingargiola <tritemio@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

import sys
import os.path
import imp
rootdir =  os.path.abspath(imp.find_module('pyplotsuite')[1]) + '/'
sourcedir = rootdir + 'plotfile/'

import gtk
from matplotlib.lines import Line2D
from pyplotsuite.common.gtk_generic_windows import \
         GenericMainPlotWindow, DialogWindowWithCancel,\
         GenericOpenFileDialog, GenericSecondaryWindow
from pyplotsuite.common.filedata import xyLabelFromFile
from pyplotsuite.common.version import read_version
from numpy import arange, array, min

# XML GUI Description file
gladefile = sourcedir + 'plotfile2.glade'

# Minimum default axis value in log scale when the original value was <= 0
LOGMIN = 1e-3

# How many centimeter is an inch
CM_PER_INCHES = 2.54

class DataSet:
    """An object describing data series with a shared X axis."""
    def __init__(self, x=None, y1=None, name=None):
        # self.x is the X axis shared for all y series
        self.x = x
        self.len = len(x)
        self.name = name

        # self.y is a list of series of data
        self.y = []
        if y1 != None: self.add(y1)

        # self.yline is a list of plotted lines from the current DataSet
        self.yline = []

        # Set the plot style, maker and linestyle attributes
        # Marker and linestyle are separate variables so we can switch them off
        # and then recover the style information when we turn them on
        self.marker = '.'
        self.linestyle = '-'

    def add(self, yi):
        if len(yi) != self.len:
            raise IndexError, 'Y data length (%d) must be == len(x) (%d)'\
                    % (len(yi), self.len)
        else:
            self.y.append(yi)

class AxesProperty:
    """Simple struct to store any properties that has different values for X
    and Y axes"""
    x = None
    y = None

##
#  Main window class
#
class PlotFileApp(GenericMainPlotWindow):
    """
    This class implements an interactive plot windows with various options.
    """
    def __init__(self, x=None, y=None, title='Title', debug=False):
        GenericMainPlotWindow.__init__(self, 'PlotFileWindow', gladefile)

        self.set_defaults()
        self.title = title
        self.debug = debug
        self.negative = False

        self.setup_gui_widgets()

        # Data: self.data is a list of DataSet() instances
        if  x != None:
            self.data.append( DataSet(x, y, 'Plot 1') )
            if min(y) <= 0:
                self.negative = True

        # Try to load the file passed as parameter
        if x == None and y == None:
            try:
                self.load(sys.argv[1])
            except:
                pass
        self.plot()
        #
        self.open_file_dialog = OpenFileDialog(self)

    def set_defaults(self):
        # Clear data
        self.data = []

        # Plot Defaults
        self.title = 'Title'
        self.xlabel = 'X Axis'
        self.ylabel = 'Y Axis'
        self.xscale = 'linear'
        self.yscale = 'linear'
        self.showPoints = True
        self.showLines = True
        self.grid = True

        # Reset ranges
        self.xmin, self.xmax, self.ymin, self.ymax = None, None, None, None
        self.axes_limits = AxesProperty()
        self.axes_limits.x = dict(linear=None, log=None)
        self.axes_limits.y = dict(linear=None, log=None)

    def reset_scale_buttons(self):
        self.setup = True
        if self.xscale == 'log':
            self.xscaleCheckB.set_active(True)
        else:
            self.xscaleCheckB.set_active(False)
        if self.yscale == 'log':
            self.yscaleCheckB.set_active(True)
        else:
            self.yscaleCheckB.set_active(False)
        self.setup = False

    def setup_gui_widgets(self):
        # Create the text entry handler
        self.xminEntry =  self.widgetTree.get_widget('xmin_entry')
        self.xmaxEntry =  self.widgetTree.get_widget('xmax_entry')
        self.yminEntry =  self.widgetTree.get_widget('ymin_entry')
        self.ymaxEntry =  self.widgetTree.get_widget('ymax_entry')

        # Initialize the check buttons to the correct value
        self.pointsCheckB = self.widgetTree.get_widget('points_chk_butt')
        self.linesCheckB = self.widgetTree.get_widget('lines_chk_butt')
        self.xscaleCheckB = self.widgetTree.get_widget('xlog_chk_butt')
        self.yscaleCheckB = self.widgetTree.get_widget('ylog_chk_butt')
        self.reset_scale_buttons()

        self.cooPickerCheckB = self.widgetTree.get_widget('coordinatePicker')
        self.moreGridCheckB = self.widgetTree.get_widget('moreGrid')

        self.window.show_all()
        if self.debug: print 'end of setup_gui_widgets()'

    def is_ydata_positive(self):
        for d in self.data:
            for yi in d.y:
                if (yi <= 0).any(): return False
        return True

    def plot_data(self, n=0):
        """Plot the n-th data from the self.data list of DataSet()."""

        if self.debug: print 'plot_data method'

        d = self.data[n]
        d.yline = range(len(d.y))
        for i, yi in enumerate(d.y):
            d.yline[i], = self.axis.plot(d.x, yi, ls=d.linestyle,
                marker=d.marker)

        self.axis.set_title(self.title)
        self.axis.set_xlabel(self.xlabel)
        self.axis.set_ylabel(self.ylabel)

        self.set_xlim()
        self.set_ylim()
        self.canvas.draw()

        #self.update_axis_limits_entry()

    def update_axis_limits_entry(self):

        xmin, xmax = self.axis.get_xlim()
        self.xminEntry.set_text(str(xmin))
        self.xmaxEntry.set_text(str(xmax))

        ymin, ymax = self.axis.get_ylim()
        self.yminEntry.set_text(str(ymin))
        self.ymaxEntry.set_text(str(ymax))


    def plot(self):
        if self.debug: print 'plot method'

        if self.yscale == 'log' and not self.is_ydata_positive():
            self.writeStatusBar('WARNING: Negative Y values in log scale.')

        self.axis.clear()

        self.axis.set_yscale('linear')
        self.axis.set_xscale('linear')

        for d in self.data:
            d.yline = range(len(d.y))
            for i, yi in enumerate(d.y):
                d.yline[i], = self.axis.plot(d.x, yi, ls=d.linestyle,
                        marker=d.marker)

        self.axis.set_title(self.title)
        self.axis.set_xlabel(self.xlabel)
        self.axis.set_ylabel(self.ylabel)
        self.axis.grid(self.grid)

        print 'x', self.xscale, 'y', self.yscale
        self.axis.set_yscale(self.yscale)
        self.axis.set_xscale(self.xscale)

        if len(self.data) > 0:
            self.set_xlim()
            if len(self.data[0].y) > 0:
                self.set_ylim()

        self.canvas.draw()

    def set_xlim(self):
        if self.axes_limits.x[self.xscale] == None:
            if self.debug: print 'autoscaling...'
            self.axis.autoscale_view(scalex=True, scaley=False)
            self.xmin, self.xmax = self.axis.get_xlim()
        else:
            if self.debug: print 'using old axis limit', self.axes_limits.x
            self.xmin, self.xmax = self.axes_limits.x[self.xscale]
            if self.xscale == 'log':
                if self.xmin <= 0: self.xmin = LOGMIN
                if self.xmax <= self.xmin: self.xmax = self.xmin+1
            self.axis.set_xlim(xmin=self.xmin, xmax=self.xmax)

        if self.debug: print 'xmin:', self.xmin, 'xmax:', self.xmax
        self.xminEntry.set_text(str(self.xmin))
        self.xmaxEntry.set_text(str(self.xmax))

    def set_ylim(self):
        if self.axes_limits.y[self.yscale] == None:
            if self.debug: print 'autoscaling...'
            self.axis.autoscale_view(scaley=True, scalex=False)
            self.ymin, self.ymax = self.axis.get_ylim()
        else:
            if self.debug: print 'using old axis limit'
            self.ymin, self.ymax = self.axes_limits.y[self.yscale]
            if self.yscale == 'log':
                if self.ymin <= 0: self.ymin = LOGMIN
                if self.ymax <= self.ymin: self.ymax = self.ymin+1
            self.axis.set_ylim(ymin=self.ymin, ymax=self.ymax)

        if self.debug: print 'ymin:', self.ymin, 'ymax:', self.ymax
        self.yminEntry.set_text(str(self.ymin))
        self.ymaxEntry.set_text(str(self.ymax))

    def load(self, filename):
        try:
            x, y , xlab, ylab = xyLabelFromFile(filename)
        except IOError:
            self.writeStatusBar("Can't open file '%s'" % filename)
            return False
        except:
            self.writeStatusBar("File '%s': file format unknown." % filename)
            return False
        else:
            filename = os.path.basename(filename)
            self.data.append(DataSet(x,y, filename))
            self.title = filename
            if xlab is not None: self.xlabel = xlab
            if ylab is not None: self.ylabel = ylab
            self.axes_limits.x = dict(linear=None, log=None)
            self.axes_limits.y = dict(linear=None, log=None)
            self.writeStatusBar("File '%s' loaded." % filename)
            if y.min() <= 0:
                self.negative = True
            return True

    def warn(self, msg):
        self.writeStatusBar('Warning: '+msg)

    ##
    # The following are GUI callback methods
    #
    def on_xscale_toggled(self, widget, *args):
        if not self.setup:
            self.axes_limits.x[self.xscale] = self.axis.get_xlim()
        if self.xscaleCheckB.get_active(): self.xscale = 'log'
        else: self.xscale = 'linear'
        if self.debug: print "XScale:", self.xscale
        self.axis.set_xscale(self.xscale)
        self.set_xlim()
        self.canvas.draw()
        self.writeStatusBar('X Axis Scale: %s.' % self.xscale)


    def on_yscale_toggled(self, widget, *args):
        if not self.setup:
            self.axes_limits.y[self.yscale] = self.axis.get_ylim()
        if self.yscaleCheckB.get_active(): self.yscale = 'log'
        else: self.yscale = 'linear'
        if self.debug: print "YScale:", self.yscale
        self.axis.set_yscale(self.yscale)
        self.set_ylim()
        self.canvas.draw()

        if self.negative and self.yscale == 'log':
            self.warn('Negative values not displayed in log-scale, lines '+\
                        'may appear discontinuos!')
        else:
            self.writeStatusBar('Y Axis Scale: %s.' % self.yscale)


    def on_points_toggled(self, widget, *args):
        self.showPoints = not self.showPoints
        if self.debug: print "Show Points:", self.showPoints
        for d in self.data:
            if self.showPoints:
                # Restore the previous marker style
                d.yline[0].set_marker(d.marker)
            else:
                # Turn off the marker visualization
                d.yline[0].set_marker('')
        self.canvas.draw()

    def on_lines_toggled(self, widget, *args):
        self.showLines = not self.showLines
        if self.debug: print "Show Lines:", self.showLines
        for d in self.data:
            if self.showLines:
                # Restore the previous linestyle
                d.yline[0].set_linestyle(d.linestyle)
            else:
                # Turn off the line visualization
                d.yline[0].set_linestyle('')
        self.canvas.draw()

    def on_grid_toggled(self, widget, *args):
        self.grid = not self.grid
        if self.debug: print "Show Grid:", self.grid
        self.axis.grid(self.grid)
        self.canvas.draw()

    def on_xmin_entry_activate(self, widget, *args):
        if self.debug: print 'X Min Entry Activated'
        s = self.xminEntry.get_text()
        try:
            val = float(s)
        except ValueError:
            self.statusBar.push(self.context, 'Wrong X axis min limit.')
            self.xminEntry.set_text('')
        else:
            if self.xscale == 'log' and val <= 0:
                val = LOGMIN
                self.warn('Only values > 0 are allowed in log scale.')
                self.xminEntry.set_text(str(val))
            self.xmin = val
            self.axis.set_xlim(xmin=val)
            self.canvas.draw()

    def on_xmax_entry_activate(self, widget, *args):
        if self.debug: print 'X Max Entry Activated'
        s = self.xmaxEntry.get_text()
        try:
            val = float(s)
        except ValueError:
            self.statusBar.push(self.context, 'Wrong X axis max limit.')
            self.xmaxEntry.set_text('')
        else:
            if self.xscale == 'log' and val <= 0:
                val = self.xmin*10.0
                self.warn('Only values > 0 are allowed in log scale.')
                self.xmaxEntry.set_text(str(val))
            self.xmax = val
            self.axis.set_xlim(xmax=val)
            self.canvas.draw()

    def on_ymin_entry_activate(self, widget, *args):
        if self.debug: print 'Y Min Entry Activated'
        s = self.yminEntry.get_text()
        try:
            val = float(s)
        except ValueError:
            self.statusBar.push(self.context, 'Wrong Y axis min limit.')
            self.yminEntry.set_text('')
        else:
            if self.yscale == 'log' and val <= 0:
                val = LOGMIN
                self.warn('Only values > 0 are allowed in log scale.')
                self.yminEntry.set_text(str(val))
            self.ymin = val
            self.axis.set_ylim(ymin=val)
            self.canvas.draw()

    def on_ymax_entry_activate(self, widget, *args):
        if self.debug: print 'Y Max Entry Activated'
        s = self.ymaxEntry.get_text()
        try:
            val = float(s)
        except ValueError:
            self.statusBar.push(self.context, 'Wrong Y axis max limit.')
            self.ymaxEntry.set_text('')
        else:
            if self.yscale == 'log' and val <= 0:
                val = self.ymin*10.0
                self.warn('Only values > 0 are allowed in log scale.')
                self.ymaxEntry.set_text(str(val))
            self.ymax = val
            self.axis.set_ylim(ymax=val)
            self.canvas.draw()

    def on_apply_button_clicked(self, widget, *args):
        sxmin = self.xminEntry.get_text()
        sxmax = self.xmaxEntry.get_text()
        symin = self.yminEntry.get_text()
        symax = self.ymaxEntry.get_text()
        try:
            xmin_val = float(sxmin)
            xmax_val = float(sxmax)
            ymin_val = float(symin)
            ymax_val = float(symax)
        except ValueError:
            self.statusBar.push(self.context, 'Wrong axis limit')
        else:
            if self.xscale == 'log':
                if xmin_val <= 0: xmin_val = LOGMIN
                if xmax_val <= 0: xmax_val = xmin_val*10
                if ymin_val <= 0: ymin_val = LOGMIN
                if ymax_val <= 0: ymax_val = ymin_val*10
            self.xmin, self.xmax, self.ymin, self.ymax = \
                    xmin_val, xmax_val, ymin_val, ymax_val
            self.axis.set_xlim(xmin=xmin_val)
            self.axis.set_xlim(xmax=xmax_val)
            self.axis.set_ylim(ymin=ymin_val)
            self.axis.set_ylim(ymax=ymax_val)
            self.canvas.draw()

    ##
    # The following are MENU callback methods
    #

    ## Actions
    def on_addDataSet_activate(self, widget, *args):
        self.open_file_dialog.window.show()
    def on_new_activate(self, widget, *args):
        self.set_defaults()
        self.reset_scale_buttons()
        self.plot()

    ## Tools
    def on_autoscale_activate(self, widget, *args):
        self.axis.autoscale_view()
        self.canvas.draw()
    def on_coordinatePicker_activate(self, widget, *args):
        if self.cooPickerCheckB.get_active():
            self.cid = self.canvas.mpl_connect('button_press_event',
                    get_coordinates_cb)
            self.writeStatusBar('Click on the plot to log coordinates on '+\
                    'the terminal.')
        elif self.cid != None:
            self.canvas.mpl_disconnect(self.cid)
            self.writeStatusBar('Coordinate picker disabled.')
        else:
            print "Error: tried to disconnect an unexistent event."
    def on_moreGrid_activate(self, widget, *args):
        if self.moreGridCheckB.get_active():
            self.axis.xaxis.grid(True, which='minor', ls='--', alpha=0.5)
            self.axis.yaxis.grid(True, which='minor', ls='--', alpha=0.5)
            self.axis.xaxis.grid(True, which='major', ls='-', alpha=0.5)
            self.axis.yaxis.grid(True, which='major', ls='-', alpha=0.5)
        else:
            self.axis.xaxis.grid(False, which='minor')
            self.axis.yaxis.grid(False, which='minor')
            self.axis.xaxis.grid(True, which='major', ls=':')
            self.axis.yaxis.grid(True, which='major', ls=':')
        self.canvas.draw()

    ## Dialogs
    def on_SizeAndResolution_activate(self, widget, *args):
        DimentionAndResolution(self)
    def on_plot_properties_activate(self, widget, *args):
        PlotPropertiesDialog(self)
    def on_makeInterpolation_activate(self, widget, *args):
        InterpolationDialog(self)
    def on_title_and_axes_labels_activate(self, widget, *args):
        TitleAndAxesWindow(self)
    def on_info_activate(self, widget, *args):
        AboutDialog()


def get_coordinates_cb(event):
    """
    Callback for the coordinate picker tool. This function is not a class
    method because I don't know how to connect an MPL event to a method instead
    of a function.
    """
    if not event.inaxes: return
    print "%3.4f\t%3.4f" % (event.xdata, event.ydata)

##
# Abstract dialogs classes
#
class DialogWithPlotList(DialogWindowWithCancel):
    """Abstract class for dialogs with a list of current plots in them."""

    def __init__(self, dialogName, gladefile, callerApp):
        DialogWindowWithCancel.__init__(self, dialogName, gladefile, callerApp)

        self.treeView = self.widgetTree.get_widget('treeview')
        self.create_plotlist()

    def create_plotlist(self):
        # Add a column to the treeView
        self.addListColumn('Plot List', 0)

        # Create the listStore Model to use with the treeView
        self.plotList = gtk.ListStore(str)

        # Populate the list
        self.insertPlotList()

        # Attatch the model to the treeView
        self.treeView.set_model(self.plotList)

    def addListColumn(self, title, columnId):
	"""This function adds a column to the list view.
	First it create the gtk.TreeViewColumn and then set
	some needed properties"""

	column = gtk.TreeViewColumn(title, gtk.CellRendererText(), text=0)
	column.set_resizable(True)
	column.set_sort_column_id(columnId)
	self.treeView.append_column(column)

    def insertPlotList(self):
        for data in self.callerApp.data:
            self.plotList.append([data.name])

    def on_applyButton_clicked(self, widget, *args):
        self.window.destroy()

    def on_cancelButton_clicked(self, widget, *args):
        self.window.destroy()

    def on_okButton_clicked(self, widget, *args):
        self.window.destroy()

##
# Dialogs classes
#
class InterpolationDialog(DialogWithPlotList):
    def __init__(self, callerApp):
        DialogWithPlotList.__init__(self, 'InterpolationDialog',
                gladefile, callerApp)
        # To be implemented ...


class PlotPropertiesDialog(DialogWithPlotList):

    colors = 'bgrcmyk'

    line_styles = ['-', '--', ':', '-.']
    line_stylesd = {'Continuous': '-', 'Dashed': '--', 'Dotted': ':',
            'Dot-Line': '-.'}

    markers = 'os^v<>.+xdDhHp'

    def __init__(self, callerApp):
        DialogWithPlotList.__init__(self, 'PlotPropertiesDialog',
                gladefile, callerApp)

        if not self.callerApp.linesCheckB.get_active():
            self.callerApp.linesCheckB.set_active(True)

        if not self.callerApp.pointsCheckB.get_active():
            self.callerApp.pointsCheckB.set_active(True)

        self.load_widgets()

        # Retrive the data list
        self.data = self.callerApp.data

        # Save all the lines properties in case of Cancell-button
        self.orig_lines = range(len(self.data))
        for i,d in enumerate(self.data):
            self.orig_lines[i] = [Line2D([0], [0]), d.linestyle, d.marker]
            self.orig_lines[i][0].update_from(d.yline[0])

        self.selected_data = None
        self.selected_line = None
        self.allowReplot = False

    def load_widgets(self):
        self.lineWidthSpinButt = \
                self.widgetTree.get_widget('lineWidthSpinButton')
        self.lineStyleComboB = \
                self.widgetTree.get_widget('lineStyleComboBox')
        self.colorComboB = \
                self.widgetTree.get_widget('lineColorComboBox')
        self.markerComboB = \
                self.widgetTree.get_widget('markerTypeComboBox')
        self.markerSizeSpinButt = \
                self.widgetTree.get_widget('markerSizeSpinButton')
        self.markerEdgeColorComboB = \
                self.widgetTree.get_widget('markerEdgeColComboBox')
        self.markerFaceColorComboB = \
                self.widgetTree.get_widget('markerFaceColComboBox')
        self.markerEdgeWidthSpinButt = \
                self.widgetTree.get_widget('markerEdgeWidthSpinButton')

    def update_properties(self):
        selected_line = self.selected_line

        # Set the Spin Buttons with the values for the selected data
        self.lineWidthSpinButt.set_value(selected_line.get_linewidth())
        self.markerSizeSpinButt.set_value(selected_line.get_markersize())
        mew = selected_line.get_markeredgewidth()
        self.markerEdgeWidthSpinButt.set_value(mew)

        # Set the Combo Boxes with the values for the selected data
        self.set_combobox(self.lineStyleComboB, selected_line.get_linestyle,
                self.line_styles)
        self.set_combobox(self.colorComboB, selected_line.get_color,
                self.colors)
        self.set_combobox(self.markerComboB, selected_line.get_marker,
                self.markers)
        self.set_combobox(self.markerEdgeColorComboB, selected_line.get_mec,
                self.colors)
        self.set_combobox(self.markerFaceColorComboB, selected_line.get_mfc,
                self.colors)

    def set_combobox(self, cbox, retrive_fun, choses):
        value = retrive_fun()
        print choses, value

        if value in choses:
            i = choses.index(value)
            cbox.set_active(i)
        else:
            print "WARNING: Unsupported value '%s'." % value


    def set_line_prop_sbut(self, prop):
        value = getattr(self, prop+'SpinButt').get_value()
        self.set_line_prop(prop, value)

    def set_line_prop_cbox(self, prop, choses=None):

        text = getattr(self, prop+'ComboB').get_active_text()

        if choses == None:
            value = text[text.find('(') + 1]
        elif text in choses.keys():
            value = choses[text]
        else:
            print "WARNING: Unsupported value '%s'." % value

        self.set_line_prop(prop, value)

    def set_line_prop(self, prop, value):
        if not self.allowReplot or self.selected_data is None: return

        set_fun = getattr(self.selected_line, 'set_'+prop.lower())
        set_fun(value)
        self.callerApp.canvas.draw()


    ##
    # GUI Callbacks
    #
    def on_treeview_cursor_changed(self, *args):
        # Retrive the current plot/line reference
        plot_index = self.treeView.get_cursor()[0][0]
        self.selected_line = self.data[plot_index].yline[0]
        self.selected_data = self.callerApp.data[plot_index]

        # Update the plot with the current line/plot properties
        self.allowReplot = False
        self.update_properties()
        self.allowReplot = True

    def on_lineWidth_value_changed(self, *args):
        self.set_line_prop_sbut('lineWidth')

    def on_lineStyle_changed(self, *args):
        self.set_line_prop_cbox('lineStyle', choses=self.line_stylesd)

    def on_lineColor_changed(self, *args):
        self.set_line_prop_cbox('color')

    def on_markerType_changed(self, *args):
        self.set_line_prop_cbox('marker')

    def on_markerSize_value_changed(self, *args):
        self.set_line_prop_sbut('markerSize')

    def on_markerEdgeWidth_value_changed(self, *args):
        self.set_line_prop_sbut('markerEdgeWidth')

    def on_markerEdgeColor_changed(self, *args):
        self.set_line_prop_cbox('markerEdgeColor')

    def on_markerFaceColor_changed(self, *args):
        self.set_line_prop_cbox('markerFaceColor')


    def on_cancelButton_clicked(self, widget, *args):
        # Restore the old style
        for orig_line, d in zip(self.orig_lines, self.data):
            d.yline[0].update_from(orig_line[0])
            d.linestyle = orig_line[1]
            d.marker = orig_line[2]

        self.callerApp.canvas.draw()
        self.window.destroy()

    def on_okButton_clicked(self, widget, *args):
        self.window.destroy()


class TitleAndAxesWindow(DialogWindowWithCancel):
    """
    Dialog for setting Title and axes labels.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'TitleAndAxesWindow', gladefile,
                callerApp)

        self.titleEntry = self.widgetTree.get_widget('titleEntry')
        self.xlabelEntry = self.widgetTree.get_widget('xlabelEntry')
        self.ylabelEntry = self.widgetTree.get_widget('ylabelEntry')

        # Update the entries with the current values
        def sync(field, entry):
            if field != None: entry.set_text(field)

        sync(self.callerApp.title, self.titleEntry)
        sync(self.callerApp.xlabel, self.xlabelEntry)
        sync(self.callerApp.ylabel, self.ylabelEntry)

    def on_okButton_clicked(self, widget, *args):
        print "OK Clicked"
        self.callerApp.axis.set_title(self.titleEntry.get_text())
        self.callerApp.axis.set_xlabel(self.xlabelEntry.get_text())
        self.callerApp.axis.set_ylabel(self.ylabelEntry.get_text())
        self.callerApp.canvas.draw()

        self.callerApp.title = self.titleEntry.get_text()
        self.callerApp.xlabel = self.ylabelEntry.get_text()
        self.callerApp.ylabel = self.xlabelEntry.get_text()

        self.window.destroy()

class DimentionAndResolution(DialogWindowWithCancel):
    """
    Dialog for setting Figure dimentions and resolution.
    """
    def __init__(self, callerApp):
        DialogWindowWithCancel.__init__(self, 'DimentionsDialog', gladefile,
                callerApp)
        self.xdimSpinButton = self.widgetTree.get_widget('xdimSpinButton')
        self.ydimSpinButton = self.widgetTree.get_widget('ydimSpinButton')
        self.resolutionSpinButton = self.widgetTree.get_widget(
                'resolutionSpinButton')
        self.inchesRadioB = self.widgetTree.get_widget('inRadioButton')

        self.xdim_o, self.ydim_o = self.callerApp.figure.get_size_inches()
        self.dpi_o = self.callerApp.figure.get_dpi()

        self.set_initial_values()

    def set_values(self, xdim, ydim, dpi):
        if not self.inchesRadioB.get_active():
            xdim *= CM_PER_INCHES
            ydim *= CM_PER_INCHES
        self.xdimSpinButton.set_value(xdim)
        self.ydimSpinButton.set_value(ydim)
        self.resolutionSpinButton.set_value(dpi)

    def set_initial_values(self):
        self.set_values(self.xdim_o, self.ydim_o, self.dpi_o)

    def set_default_values(self):
        xdim, ydim = rcParams['figure.figsize']
        dpi = rcParams['figure.dpi']
        self.set_values(xdim, ydim, dpi)

    def get_values(self):
        self.xdim = self.xdimSpinButton.get_value()
        self.ydim = self.ydimSpinButton.get_value()
        self.resolution = self.resolutionSpinButton.get_value()
        if not self.inchesRadioB.get_active():
            self.xdim /= CM_PER_INCHES
            self.ydim /= CM_PER_INCHES

    def set_figsize(self):
        self.callerApp.figure.set_size_inches(self.xdim, self.ydim)
        self.callerApp.figure.set_dpi(self.resolution)

    def on_unity_toggled(self, widget, *args):
        xdim = self.xdimSpinButton.get_value()
        ydim = self.ydimSpinButton.get_value()
        if self.inchesRadioB.get_active():
            xdim /= CM_PER_INCHES
            ydim /= CM_PER_INCHES
        else:
            xdim *= CM_PER_INCHES
            ydim *= CM_PER_INCHES
        self.xdimSpinButton.set_value(xdim)
        self.ydimSpinButton.set_value(ydim)

    def on_okButton_clicked(self, widget, *args):
        print "OK"
        self.get_values()
        self.set_figsize()
        self.callerApp.canvas.draw()
        self.window.destroy()

    def on_restoreButton_clicked(self, widget, *args):
        self.set_default_values()


class OpenFileDialog(GenericOpenFileDialog):
    """
    This class implements the "Open File" dialog.
    """
    def __init__(self, callerApp):
        GenericOpenFileDialog.__init__(self, 'OpenFileDialog', gladefile,
                callerApp)
        self.window.hide()

    def openSelectedFile(self):
        loaded = self.callerApp.load( self.filename )
        if loaded:
            self.callerApp.plot_data(-1)
        self.window.hide()

    def on_cancelButton_clicked(self, widget, *args):
        self.window.hide()
        return True



class AboutDialog(GenericSecondaryWindow):
    """
    Object for the "About Dialog".
    """
    def __init__(self):
        GenericSecondaryWindow.__init__(self, 'AboutDialog', gladefile)
        self.window.set_version(read_version(rootdir))
        self.window.connect("response", lambda d, r: d.destroy())

##
# Functions
#
def test():
    x = arange(100)
    y = sin(x/10.0)
    p = PlotFileApp(x, y, title='Random Sequence', debug=True)
    p.start()

def main():
    p = PlotFileApp(debug=True)
    # Try to open a sample file
    if len(sys.argv) < 2  and os.path.isfile(sourcedir+'samples/data.txt'):
        p.load(sourcedir+'samples/data.txt')
        p.plot()
    p.start()

if __name__ == '__main__': main()
